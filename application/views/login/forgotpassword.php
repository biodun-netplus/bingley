<div class="container" id="login-form">
    <div class="row">
        <div class="col-md-8 col-xs-12 hidden-xs login-heading">
            <div>
                <p>Welcome to</p>
            </div>
            <div>
                <p style="background: #071c0e;">Bingley Apartment residents and</p>
            </div>
            <div>
                <p style="background:#324a17;">utilities management portal</p>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div align="center">
                <img src="https://netpluspay.com/images/netpluspayLogoGreen.png" class="login-logo" style="height: 50px; width: 200px;"/>
                <br/>
            </div>

            <?php $this->load->view('includes/notification'); ?>
            <div class="panel login-panel">

                <?php echo form_open('login/forgotpass'); ?>

                <div class="panel-body">
                    <div class="form-group mb-md">
                        <div class="col-xs-12">
                            <div class="input-group">
										<span class="input-group-addon">
											<i class="ti ti-user"></i>
										</span>
                            <h5>Enter your email address below and click reset. You will recieve an email notification with your new password.</h5>
                                <input type="email" class="form-control" placeholder="Email" name="email"
                                       value="<?php echo set_value('email'); ?>" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="clearfix">
                        <button type="submit" class="btn btn-login btn-raised pull-left">Reset Password</button>
                        <a href="<?php echo site_url('/'); ?>"
                           class="btn btn-defualt">Login</a>

                    </div>
                </div>

                </form>
            </div>
        </div>
    </div>
</div>
