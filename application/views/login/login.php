<div class="container" id="login-form">
    <div class="row">
        <div class="col-md-8 col-xs-12 hidden-xs login-heading">
            <div>
                <p>Welcome to</p>
            </div>
            <div>
                <p style="background: #071c0e;">Bingley Apartment residents and</p>
            </div>
            <div>
                <p style="background:#324a17;">utilities management portal</p>
            </div>
        </div>
        <div class="col-md-4 col-xs-12">
            <div align="center">
                <img src="https://netpluspay.com/images/netpluspayLogoGreen.png" class="login-logo" style="height: 50px; width: 200px;"/>
                <br/>
            </div>

            <?php $this->load->view('includes/notification'); ?>
            <div class="panel login-panel">

                <?php echo form_open('login/auth', ['id' => "validate-form", "class" => "form-horizontal","autocomplete"=>"off"]); ?>

                <div class="panel-body">
                    <div class="form-group mb-md">
                        <div class="col-xs-12">
                            <div class="input-group">
										<span class="input-group-addon">
											<i class="ti ti-user"></i>
										</span>
                                <input type="email" class="form-control" placeholder="Email" name="email"
                                       value="<?php echo set_value('email'); ?>" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group mb-md">
                        <div class="col-xs-12">
                            <div class="input-group">
										<span class="input-group-addon">
											<i class="ti ti-key"></i>
										</span>
                                <input type="password" class="form-control" name="password"  data-parsley-minlength="8" 
                                       value="<?php echo set_value('password'); ?>" placeholder="Password">
                            </div>
                        </div>
                    </div>
                    <!--
                                   <div class="form-group mb-n">
                                       <div class="col-xs-12">

                                           <a href="forgotpassword.html" class="pull-left">Forgot password?</a>

                        </div>
                    </div>
                    -->
                </div>
                <div class="panel-footer">
                    <div class="clearfix">
                        <button type="submit" class="btn btn-login btn-raised pull-left">Login</button>
                        <a href="<?php echo site_url('registration'); ?>"
                           class="btn btn-default pull-right">Register</a>

                    </div>
                </div>
                <a href="<?php echo base_url('login/forgotpassword'); ?>" class="pull-left" style="padding-left:20px; padding-bottom:10px; color:red; font-size:15px"> Forgot password?</a>

                </form>
            </div>
        </div>
    </div>
</div>
