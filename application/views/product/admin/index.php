<div class="static-content">
<div class="page-content">
  <?php if ($this->aauth->is_member('Merchant')):?>
  <span class="pull-right" style="padding: 10px;">Meter No :
  <?= $this->aauth->get_user()->meter_no ?>
  </span>
  <?php endif; ?>
  <div class="page-heading">
    <h1>Product</h1>
    <div class="options"> </div>
  </div>
  <div class="container-fluid">
    <div data-widget-group="group1">
      <?php if(!empty($this->session->flashdata('flashMsg'))){?>
      <div class="alert alert-success">
        <button class="close" data-close="alert"></button>
        <span> <?php echo $this->session->flashdata('flashMsg')?></span> </div>
      <?php } ?>
      <?php  if ($this->aauth->is_member('Admin')) { ?>
      <div class="row"> <a href="<?php echo site_url('product/product_add/add'); ?>">
        <button class="btn btn-primary btn-raised pull-left" type="button">Add Product</button>
        </a>
        <?php } ?>
      </div>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h2>Product list</h2>
                <div class="panel-ctrls"></div>
              </div>
              <div class="panel-body no-padding">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>Product Name</th>
                      <th>Account Number</th>
                      <th>Bank Name</th>
                       <th>Property Type</th>
                        <th>Product Type</th>
                      <th>Base Charge</th>
                       <!-- <th>Take or Pay</th> -->
                        <th>Total Price</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
						$i=1;
						if(isset($records) && count($records)>0){
						foreach($records as $LoopRecord){?>
                    <tr>
                      <td> <?php echo $LoopRecord->product_name ?></td>
                      <td><?php echo $LoopRecord->account_number ?></td>
                      <td><?php echo $LoopRecord->bank_name ?></td>
                      <td> <?php echo $LoopRecord->property_type ?></td>
                      <td> <?php echo $LoopRecord->product_type ?></td >
                       <td> <?php echo $LoopRecord->base_charge ?></td>
                       <!-- <td> <?php echo $LoopRecord->take_or_pay ?></td> -->
                        <td> <?php echo $LoopRecord->product_price ?></td>
                      <td><a href="<?php echo site_url('product/product_add/edit/'.$LoopRecord->id); ?>">Edit</a>
                       |
                       <?php if(!($LoopRecord->product_type === 'Service')): ?>
                       <a href="<?php echo site_url('product/product_add/delete/'.$LoopRecord->id); ?>">Delete</a></td>
                      <?php endif; ?>
                    </tr>
                    <?php $i++;}}?>
                  </tbody>
                </table>
              </div>
              <div class="panel-footer"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .container-fluid --> 
  </div>
  <!-- #page-content --> 
</div>
