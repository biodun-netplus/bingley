<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')):?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no ?></span>
        <?php endif; ?>
        
        <div class="page-heading">
            <h1>Product Details</h1>

            
        </div>
        <div class="container-fluid">
        <div class="row"> 
        
		<?php if(!empty($this->session->flashdata('flashMsg'))){?>
                <div class="alert alert-success">
                    <button class="close" data-close="alert"></button>
                    <span> <?php echo $this->session->flashdata('flashMsg')?></span>
                </div>
           <?php } ?>
        
         <div class="row">
          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h2>Product Details</h2>
                <div class="panel-ctrls"></div>
              </div>
             
              <div class="panel-body no-padding">
              
              
              
                <table  class="table table-striped table-bordered" cellspacing="0" width="50%">
                  <thead>
                    <tr>
                      <th>Product Name</th>
                      <td><?php echo $ProductDetails['product_name']?></td>
                      
                    </tr>
                    
                    <tr>
                      <th>Base Charge</th>
                      <td><?php echo $ProductDetails['base_charge']?></td>
                      
                    </tr>
                    
                    <tr>
                      <th>Take or Pay</th>
                      <td><?php echo $ProductDetails['take_or_pay']?></td>
                      
                    </tr>
                    
                    <tr>
                      <th>Total Price</th>
                      <td><?php echo $ProductDetails['product_price']?></td>
                      
                    </tr>
                    <tr>
                      <th>Property Type</th>
                      <td><?php echo $ProductDetails['property_type']?></td>
                      
                    </tr>
                    
                    <tr>
                      <th>Product Type</th>
                      <td><?php echo $ProductDetails['product_type']?></td>
                      
                    </tr>
                    <tr>
                      <th>Partner Type</th>
                      <td><?php echo $ProductDetails['partner_type']?></td>
                      
                    </tr>
                    
                    
                   
                    <tr>
                      <th> Date</th>
                      <td><?php echo date('Y-M-d',strtotime($ProductDetails['created']));?></td>
                      
                    </tr>
                   
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
              </div>
              <div class="panel-footer"></div>
            </div>
          </div>
        </div>
        
        
        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>
                