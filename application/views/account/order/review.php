<style>
    b {
        font-weight: bold;
    }

    a.nav-link {
        padding: 10px;
    }
    .tab-content {
        padding: 30px 10px 10px 10px;
    }
    .card, .content-box {
        margin-bottom: 25px;
    }

    .card {
        background: transparent;
        border: none;
    }

</style>

<div class="main">
    <div class="breadcrumb">
        <a href="<?php echo site_url('order/list') ?>">Orders</a>
        <span class="breadcrumb-devider">/</span>
        <a href="<?php echo current_url(); ?>">Edit</a>
        <span class="breadcrumb-devider">/</span>
        <a class="active"><?php echo $order->order_id; ?></a>
    </div>
    <div class="content">
        <div class="panel">
            <div class="content-header no-mg-top">
                <i class="fa fa-newspaper-o"></i>

                <div class="content-header-title">Edit Order</div>
            </div>

            <div class="row">
                <div class="col-md-8" style="float:none;margin:auto;">
                    <?php $this->load->view('includes/notification'); ?>
                    <div class="content-box">
                        <!-- Nav tabs -->
                        <div class="card">

                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="nav-item ">
                                    <a href="#info" aria-controls="info"
                                       role="tab" data-toggle="tab" class="nav-link active">Customers
                                        Info</a>
                                </li>
                                <li role="presentation" class="nav-item">
                                    <a href="#orders" aria-controls="orders"
                                       role="tab" data-toggle="tab" class="nav-link">Ordered Items</a>
                                </li>
                                <li role="presentation" class="nav-item">
                                    <a href="#payment" aria-controls="payment"
                                       role="tab" data-toggle="tab" class="nav-link">Payment
                                    </a>
                                </li>
                                <li role="presentation" class="nav-item">
                                    <a href="#delivery" aria-controls="delivery"
                                       role="tab" data-toggle="tab" class="nav-link">Delivery
                                    </a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="info">

                                    <?php
                                    $attributes = array('id' => 'form-validate');
                                    echo form_open('order/update/customer/' . $order->order_id, $attributes);
                                    ?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input class="form-control"
                                                       data-error="Please input customer's First Name"
                                                       placeholder="Enter First Name" required="required" type="text"
                                                       name="first_name"
                                                       value="<?php echo $order->customer_firstname ?>">

                                                <div
                                                    class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <input class="form-control"
                                                       data-error="Please input customer's Last Name"
                                                       placeholder="Enter Last Name" required="required" type="text"
                                                       name="last_name" value="<?php echo $order->customer_lastname ?>">

                                                <div
                                                    class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Email address</label>
                                                <input class="form-control"
                                                       data-error="Customer's email address is invalid"
                                                       placeholder="Enter Email Address" required="required"
                                                       type="email"
                                                       name="email" value="<?php echo $order->contact_email; ?>">

                                                <div
                                                    class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Phone Number</label>
                                                <input class="form-control"
                                                       data-error="Please input customer's Phone Number"
                                                       placeholder="Enter Phone Number" required="required" type="text"
                                                       name="phone"
                                                       value="<?php echo $order->contact_phone ?>">

                                                <div
                                                    class="help-block form-text with-errors form-control-feedback"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <button class="btn btn-success pull-right" type="submit"><i
                                                    class="fa fa-check"></i>
                                                Update Information
                                            </button>
                                        </div>
                                    </div>
                                    <?php echo form_close(); ?>

                                </div>
                                <div role="tabpanel" class="tab-pane" id="orders">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                               <table class="table">
                                                    <thead>
                                                    <th>Product</th>
                                                    <th>Price</th>
                                                    </thead>
                                                    <?php foreach ($order_items as $item): ?>
                                                        <?php foreach ($item as $product => $price): ?>
                                                            <tr>
                                                                <td>
                                                                    <?php echo $product; ?>
                                                                </td>
                                                                <td>
                                                                    <?php echo number_format($price); ?>
                                                                </td>
                                                            </tr>
                                                        <?php endforeach; ?>
                                                    <?php endforeach; ?>
                                                </table>

                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <p style="padding: 10px;font-weight: bold;text-align: right;">Total :
                                                NGN<?php echo number_format($order->amount, 2); ?></p>

                                        </div>

                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="payment">

                                    <?php if (!empty($order->transaction_id)): ?>
                                        <h2 style="font-size: 20px;margin: 0px 10px 10px 0px;">Transaction Details</h2>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label><b>Transaction ID</b></label>

                                                    <p>
                                                        <?php echo $order->transaction_id; ?>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label><b>Status</b></label>

                                                    <p>
                                                        <?php echo $order->status; ?>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label><b>Amount Paid</b></label>

                                                    <p>
                                                        <?php echo number_format($order->amount_paid, 2); ?>
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label><b>Payment Description</b></label>

                                                    <p>
                                                        <?php echo $order->payment_description; ?>
                                                    </p>
                                                </div>
                                            </div>


                                        </div>
                                    <?php else: ?>
                                        <p>Payment Status : <b>Pending</b></p>
                                    <?php endif; ?>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="delivery">
                                    <div id="delivery-form" class="row">
                                        <?php
                                        $attributes = array('id' => 'form-validate');
                                        echo form_open('order/update/delivery/' . $order->order_id, $attributes);
                                        ?>
                                        <div id="is-delivery" class="row col-sm-12">

                                            <div class="col-sm-6 delivery">
                                                <div class="form-group">
                                                    <label>Delivery Street</label>
                                                    <input class="form-control"
                                                           data-error="Please input customer's delivery street"
                                                           placeholder="Enter Delivery Street" required="required"
                                                           type="text"
                                                           name="delivery_street"
                                                           value="<?php echo $order->delivery_street ?>">

                                                    <div
                                                        class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 delivery">
                                                <div class="form-group">
                                                    <label>Delivery City</label>
                                                    <input class="form-control"
                                                           data-error="Please input customer's delivery city"
                                                           placeholder="Enter Delivery City" required="required"
                                                           type="text"
                                                           name="delivery_city"
                                                           value="<?php echo $order->delivery_city ?>">

                                                    <div
                                                        class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 delivery">
                                                <div class="form-group">
                                                    <label>Delivery State</label>
                                                    <input class="form-control"
                                                           data-error="Please input customer's delivery state"
                                                           placeholder="Enter Delivery State" required="required"
                                                           type="text"
                                                           name="delivery_state"
                                                           value="<?php echo $order->delivery_state ?>">

                                                    <div
                                                        class="help-block form-text with-errors form-control-feedback"></div>
                                                </div>
                                            </div>
                                            <?php if(!empty($order->saddle_delivery_id)): ?>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Saddle Delivery ID</label>

                                                    <p><b><?php echo $order->saddle_delivery_id ?></p></b>

                                                </div>
                                            </div>
                                            <?php endif; ?>

                                        </div>


                                        <div class="col-sm-12">

                                            <button class="btn btn-success pull-right" type="submit"><i
                                                    class="fa fa-check"></i>
                                                Update Delivery
                                            </button>
                                        </div>
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>