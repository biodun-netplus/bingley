            <div class="main">
                <div class="breadcrumb">
                    <a href="">Profile</a>
                    <span class="breadcrumb-devider">/</span>
                    <a href="<?php echo current_url(); ?>">Change Password</a>
                </div>
                <div class="content">
                    <div class="panel">
                        <div class="content-header no-mg-top">
                            <i class="fa fa-newspaper-o"></i>
                            <div class="content-header-title">Change Password</div>
                        </div>
                        
                        <div class="row" >
                            <div class="col-md-8" style="float:none;margin:auto;">
                                <?php $this->load->view('includes/notification'); ?>
                                <div class="content-box">
									<div class="content-box-header">
										<div class="box-title">Change Password</div>
									</div>
									<?php
                                        $attributes = array( 'id' => 'form-validate');
                                        echo form_open('dashboard/change/password', $attributes);
                                    ?>    <div class="row">
											<div class="col-sm-12">
												<div class="form-group">
                                                    <label>Currrent Password</label>
                                                    <input class="form-control" data-error="Please input current password" placeholder="Enter Current Password" required="required" type="password" name="current_password">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
                                                    <label>New Password</label>
                                                    <input class="form-control" data-error="Please input new password" id="inputPassword" placeholder="Enter New Password" required="required" type="password"  name="password">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
                                            <div class="col-sm-6">
												<div class="form-group">
                                                    <label>Confirm New Password</label>
                                                    <input class="form-control"  name="confirm_password" data-error="confirm new password is required" placeholder="Confirm New Password" required="required" type="password" data-match="#inputPassword" data-match-error="Whoops, password don't match">
                                                    <div class="help-block form-text with-errors form-control-feedback"></div>
												</div>
											</div>
										</div>
                                     
                                        
                                        
										<div class="content-box-footer">
											<button class="btn btn-primary" type="submit"><i class="fa fa-pencil"></i> Change</button>
										</div>
									</form>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
