

<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')):?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no ?></span>
        <?php endif; ?>
        <ol class="breadcrumb">

            <li class=""><a href="index.html">Home</a></li>
            <li class="active"><a href="index.html">Dashboard</a></li>

        </ol>
        <div class="page-heading">
            <h1>Dashboard</h1>

            <div class="options">

            </div>
        </div>
        <div class="container-fluid">
          
			
            <div data-widget-group="group1">
            
            <?php if ($this->aauth->is_member('Public')) {?>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <a href="<?php echo site_url('product/product'); ?>" class="btn btn-info btn-raised">Pay Bills</a>
                </div>
            </div>
			<div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="info-tile info-tile-alt tile-indigo">
                            <div class="info">
                                <div class="tile-heading"><span>Number of Payments</span></div>
                                <div class="tile-body"><span><?= $payments ?></span></div>
                            </div>
                            <div class="stats">
                                <div class="tile-content">
                                    <div id="dashboard-sparkline-indigo"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="info-tile info-tile-alt tile-primary">
                            <div class="info">
                                <div class="tile-heading"><span>Total Payments</span></div>
                                <div class="tile-body "><span>&#x20A6;<?= number_format($total_payments) ?></span></div>
                            </div>
                            <div class="stats">
                                <div class="tile-content">
                                    <div id="dashboard-sparkline-primary"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="info-tile info-tile-alt tile-danger">
                            <div class="info">
                                <div class="tile-heading"><span>Amount Due</span></div>
                                <div class="tile-body ">
                                    <span>&#x20A6;<?php echo number_format($dueAmount); ?></span>
                                </div>
                            </div>
                            <div class="stats">
                                <div class="tile-content">
                                    <div id="dashboard-sparkline-gray"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                   
                   
                    <?php if ($this->aauth->is_member('Admin')): ?>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <div class="info-tile info-tile-alt tile-success clearfix">
                                <div class="info">
                                    <div class="tile-heading"><span>Users</span></div>
                                    <div class="tile-body "><span><?= $users; ?></span>
                                    </div>
                                </div>
                                <div class="stats">
                                    <div class="tile-content">
                                        <div id="dashboard-sparkline-success"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
            </div>	
			<?php	}
				
				else{?>
            	
                <div class="row">

                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="info-tile info-tile-alt tile-indigo">
                            <div class="info">
                                <div class="tile-heading"><span>Payments</span></div>
                                <div class="tile-body"><span><?= $payments ?></span></div>
                            </div>
                            <div class="stats">
                                <div class="tile-content">
                                    <div id="dashboard-sparkline-indigo"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="info-tile info-tile-alt tile-primary">
                            <div class="info">
                                <div class="tile-heading"><span>Total Payments</span></div>
                                <div class="tile-body "><span>&#x20A6;<?= number_format($total_payments) ?></span></div>
                            </div>
                            <div class="stats">
                                <div class="tile-content">
                                    <div id="dashboard-sparkline-primary"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if ($this->aauth->is_member('Merchant')){ ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <div class="info-tile info-tile-alt tile-success clearfix">
                            <div class="info">
                                <div class="tile-heading"><span>Last Payment</span></div>
                                <div class="tile-body "><span>&#x20A6;<?= number_format($last_payment) ?></span></div>
                            </div>
                            <div class="stats">
                                <div class="tile-content">
                                    <div id="dashboard-sparkline-success"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                    <?php if ($this->aauth->is_member('Admin')){ ?>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                            <div class="info-tile info-tile-alt tile-success clearfix">
                                <div class="info">
                                    <div class="tile-heading"><span>Users</span></div>
                                    <div class="tile-body "><span><?= $users; ?></span>
                                    </div>
                                </div>
                                <div class="stats">
                                    <div class="tile-content">
                                        <div id="dashboard-sparkline-success"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php }?>


                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-controls dropdown">
                                <button class="btn btn-icon-rounded refresh-panel"><span
                                        class="material-icons inverted">refresh</span></button>

                            </div>
                            <div class="panel-body no-padding table-responsive">
                                <div class="p-md">
                                    <h4 class="mb-n">Recent payments
                                       <!-- <small>Recent payments</small>-->
                                    </h4>
                                </div>
                                <div class="list-group">
                                    <?php foreach ($recent_payments as $payment):
									//echo "<pre>";
									//print_r($payment);
									 ?>
                                        <div class="list-group-separator"></div>
                                        <div class="list-group-item withripple">
                                            <div class="row-action-primary">
                                               <span class="icon"><i
                                                class="material-icons">account_balance_wallet</i></span>
                                            </div>
                                            <div class="row-content">
                                                <div class="least-content">
                                                    <span
                                                        class="badge badge-<?php echo ($payment->status === 'Paid')? 'success' : 'danger';  ?>
                                                        "><?php echo $payment->status ?></span>
                                                        <?php if ($this->aauth->is_member('Admin')):?>
                                                            <p><b> &#x20A6;<?php echo number_format($payment->amount); ?></b></p>
                                                        <?php endif; ?>
                                                </div>
                                                <h5 class="list-group-item-sub-heading">
												<a href="<?php echo base_url()?>product/product_details/<?php echo $payment->product_id; ?>">
												<?php echo $payment->product_name; ?></a></h5>
                                                <p><?php echo $payment->full_name; ?></p>


                                                <p class="list-group-item-text"><?php echo $payment->narration ?></p>
                                            </div>
                                        </div>

                                    <?php endforeach; ?>

                                </div>

                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-controls dropdown">
                                <button class="btn btn-icon-rounded refresh-panel"><span
                                        class="material-icons inverted">refresh</span></button>

                            </div>
                            <div class="panel-body no-padding table-responsive">
                                <div class="p-md">
                                    <h4 class="mb-n">Recent Registered Users
                                      <!--  <small>Recent payments</small>-->
                                    </h4>
                                </div>
                                <div class="list-group">
                                    <?php if($userList > 0) 
                                    
                                    {?>
                                    <?php foreach ($userList as $userData): ?>
                                        <div class="list-group-separator"></div>
                                        <div class="list-group-item withripple">
                                           
                                            <div class="row-content">
                                                <div class="least-content">
                                                  
                                                        
                                                </div>
                                                
                                                  <p> Name : <?php echo $userData->full_name?> </p>
                                                  <?php /*?> <p> Address : <?php echo $userData->house_address?> </p>
                                                    <p> Mobile Number : <?php echo $userData->mobile_no?> </p><?php */?>
                                                     <p> Date of Registration : <?php echo date('Y-M-d',strtotime($userData->date_created));?> </p>
                                               
                                            </div>
                                        </div>

                                    <?php endforeach; ?>
									<?php }?>
                                </div>

                            </div>
                        </div>
                    </div>

                   
                </div>
                <div class="row">
                <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-controls dropdown">
                                <button class="btn btn-icon-rounded refresh-panel"><span
                                        class="material-icons inverted">refresh</span></button>

                            </div>
                            <div class="panel-body no-padding table-responsive">
                                <div class="p-md">
                                    <h4 class="mb-n">Outstanding Payments
                                       <!-- <small>Recent payments</small>-->
                                    </h4>
                                </div>
                                <div class="list-group">
                                    <?php foreach ($outstanding_payments as $payment):
									//echo "<pre>";
									//print_r($payment);
									 ?>
                                        <div class="list-group-separator"></div>
                                        <div class="list-group-item withripple">
                                            <div class="row-action-primary">
                                               <span class="icon"><i
                                                class="material-icons">account_balance_wallet</i></span>
                                            </div>
                                            <div class="row-content">
                                                <div class="least-content">
                                                    <span
                                                        class="badge badge-<?php echo ($payment->status === '1')? 'success' : 'danger';  ?>
                                                        "><?php echo ($payment->status === '1')? 'paid' : 'pending'; ?></span>
                                                        <?php if ($this->aauth->is_member('Admin')):?>
                                                            <p><b> &#x20A6;<?php echo number_format($payment->amount); ?></b></p>
                                                        <?php endif; ?>
                                                </div>
                                                <h5 class="list-group-item-sub-heading">
												<!-- <a href="<?php echo base_url()?>product/product_details/<?php echo $payment->product_id; ?>"> -->
												<?php echo $payment->product_name; ?></a></h5>
                                                <p><?php echo $payment->full_name; ?></p>


                                                <p class="list-group-item-text"><?php echo $payment->narration ?></p>
                                            </div>
                                        </div>

                                    <?php endforeach; ?>

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
           <?php } ?>
            </div>


        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>
                