<style>
    .select2-container--default .select2-selection--single {
        border: 1px solid rgba(0, 0, 0, .15);
        height: 33px;
        border-bottom-left-radius: 0;
        border-top-left-radius: 0;
    }
</style>

<div class="main">
    <div class="breadcrumb">
        <a href="#">Report</a>
        <span class="breadcrumb-devider">/</span>
        <a href="#">Order</a>
    </div>
    <div class="content">
        <div class="panel">
            <div class="content-header no-mg-top">
                <i class="fa fa-newspaper-o"></i>

                <div class="content-header-title">Order Report</div>
            </div>
            <form method="get">
                <div class="row">
                    <div class="<?php echo ($this->aauth->is_member('Admin')) ? 'col-md-3' : 'col-md-4'; ?>">
                        <div class="content-box">
                            <div class="form-group">
                                <label for=""> Search Query</label>

                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-search"></i></div>
                                    <input class="form-control" name="filter_query" type="text"
                                           value="<?php echo $filter_query; ?>" placeholder="Search">
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="<?php echo ($this->aauth->is_member('Admin')) ? 'col-md-3' : 'col-md-4'; ?>">
                        <div class="content-box">
                            <div class="form-group">
                                <label for=""> Filter By Date</label>

                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <input id="multi-daterange" class="multi-daterange form-control" name="filter_date"
                                           type="text" value="<?php echo $filter_date; ?>">
                                </div>

                            </div>
                        </div>
                    </div>
                    <?php if ($this->aauth->is_member('Admin')): ?>
                        <div class="<?php echo ($this->aauth->is_member('Admin')) ? 'col-md-3' : 'col-md-4'; ?>">
                            <div class="content-box">
                                <div class="form-group">
                                    <label for=""> Filter By Merchant</label>

                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-users"></i></div>
                                        <select class="js-example-basic-single" name="filter_merchant">
                                            <option value=""></option>
                                            <?php foreach ($merchants as $merchant): ?>
                                                <option
                                                    value="<?php echo $merchant->company_name; ?>" <?php echo ($filter_merchant === $merchant->company_name) ? 'selected' : ''; ?>><?php echo $merchant->company_name; ?></option>
                                            <?php endforeach; ?>
                                        </select>

                                    </div>

                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="<?php echo ($this->aauth->is_member('Admin')) ? 'col-md-3' : 'col-md-4'; ?>">
                        <div class="content-box">
                            <div class="form-group">
                                <label for=""> Filter By Status</label>

                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-cubes"></i></div>
                                    <select class="form-control" name="filter_status">
                                        <option value=""></option>
                                        <option
                                            value="Pending" <?php echo ($this->input->get('filter_status') == 'Pending') ? 'selected' : ''; ?>>
                                            Pending
                                        </option>
                                        <option
                                            value="Completed" <?php echo ($this->input->get('filter_status') == 'Completed') ? 'selected' : ''; ?>>
                                            Completed
                                        </option>
                                        <option
                                            value="Cancelled" <?php echo ($this->input->get('filter_status') == 'Cancelled') ? 'selected' : ''; ?>>
                                            Cancelled
                                        </option>
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 sm-max sm-text-center">
                        <div style="margin-top:10px;">
                            <a class="btn btn-warning pull-right" href="<?php echo base_url('order/list') ?>"
                               style="margin-left:10px;"><i class="fa fa-remove"></i> Reset</a>
                            <button class="btn btn-info sm-max  pull-right" type="submit"><i class="fa fa-search"></i>
                                Search
                            </button>


                        </div>
                    </div>

                </div>
            </form>
            <div class="row">
                <div class="col-md-12">
                    <?php $this->load->view('includes/notification'); ?>
                    <div class="content-box">
                        <div class="content-box-header">
                            <div class="row">
                                <form method="get" action="<?php echo site_url('order/order_export'); ?>">
                                    <input type="hidden" name="filter_status" value="<?php echo $this->input->get('filter_status'); ?>"/>
                                    <input type="hidden" name="filter_merchant" value="<?php echo $this->input->get('filter_merchant'); ?>"/>
                                    <input type="hidden" name="filter_date" value="<?php echo $this->input->get('filter_date'); ?>"/>
                                    <input type="hidden" name="filter_query" value="<?php echo $this->input->get('filter_query'); ?>"/>
                                    <div class="col-md-12 sm-max sm-text-center">
                                        <button class="btn btn-primary sm-max pull-right" type="submit"><i
                                                class="fa fa-download"></i> Export</button>
                                    </div>
                                </form>

                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Order ID</th>
                                    <?php if ($this->aauth->is_member('Admin')): ?>
                                        <th>Merchant Account</th>
                                    <?php endif; ?>
                                    <th>Customer Name</th>
                                    <th>Email</th>
                                    <th>Order Date</th>
                                    <th>Status</th>
                                    <th>Order Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (count($orders) > 0): ?>

                                    <?php foreach ($orders as $order): ?>
                                        <tr>
                                            <th><?php echo $order->order_id; ?></th>
                                            <?php if ($this->aauth->is_member('Admin')): ?>
                                                <th><?php echo $order->company_name; ?></th>
                                            <?php endif; ?>
                                            <td><?php echo $order->customer_firstname . ' ' . $order->customer_lastname; ?></td>
                                            <td><?php echo $order->contact_email; ?></td>
                                            <td><?php echo $order->date_created; ?></td>
                                            <td><?php echo $order->status; ?></td>
                                            <td><?php echo '<b style="font-weight: bold;">' . $order->currency . '</b>' . number_format($order->amount, 2); ?></td>
                                        </tr>
                                    <?php endforeach; ?>

                                <?php else: ?>
                                    <tr>
                                        <td colspan="8" class="text-center">No results found</td>
                                    </tr>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="content-box-footer">
                            <div class="row">

                                <div class="col-md-12 sm-max">
                                    <ul class="pagination sm-mgtop-5 pull-right ">
                                        <?php foreach ($links as $link) {
                                            echo $link;
                                        } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#multi-daterange').daterangepicker({"startDate": "<?php echo $from ?>", "endDate": "<?php echo $to ?>"});
        $(".js-example-basic-single").select2();
    });
</script>