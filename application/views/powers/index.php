
<div class="static-content">
<div class="page-content">
  <?php if ($this->aauth->is_member('Merchant')):?>
  <span class="pull-right" style="padding: 10px;">Meter No :
  <?= $this->aauth->get_user()->meter_no ?>
  </span>
  <?php endif; ?>
  <div class="page-heading">
    <h1>Power</h1>
    <div class="options"> </div>
  </div>
  <div class="container-fluid">
    <?php $this->load->view('includes/notification'); ?>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <a href="<?php echo site_url('power/create'); ?>" class="btn btn-info btn-raised">Vend Power</a>

            <div class="panel panel-default">
              <div class="panel-heading">
                <h2>Power Vending List</h2>
                <div class="panel-ctrls"></div>
              </div>
             
              <div class="panel-body">
                                <table id="defaultTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>User</th>
                      <th>Meter No</th>
                      <th>Amount</th>
                      <th>Status</th>
                      <th>Token</th>
        
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($payments as $payment):?>
                    <tr>
                     
                        <td><?php echo $payment->full_name; ?></td>
                        <td><?php echo $payment->meter_no; ?></td>
                        <td><?php echo $payment->amount_paid; ?></td>
                        <td><?php echo $payment->status; ?></td>
                        <td><?php echo $payment->token_no; ?></td>
                     
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>

              </div>
                        
             
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- .container-fluid --> 
  </div>
  <!-- #page-content --> 
</div>


</body>