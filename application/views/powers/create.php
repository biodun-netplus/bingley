<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')):?>
            <span class="pull-right" style="padding: 10px;">Meter No : <?= $this->aauth->get_user()->meter_no ?></span>
        <?php endif; ?>
       
        <div class="page-heading">
            <h1>Vend Power</h1>

            <div class="options">

            </div>
        </div>
        <div class="container-fluid">

        <?php $this->load->view('includes/notification'); ?>
            <div class="col-sm-6" style="float:none;margin:auto;">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2>Create Vend Power</h2>
                    </div>
                  

                    <div class="panel-body">
                        <div class="form-group mb-md">
                             
                            <div class="col-xs-8">
                                <label>Assign User</label>
                                <select class="form-control" id="select1" name="user" required>
                                    <?php foreach($users as $user): ?>
                                    <option value="<?= $user->id ?>"><?= $user->full_name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            
                        </div>

                        <div class="col-xs-4">
                            <input type="button" value="Verify" id="verify" class="btn btn-raised btn-success pull-right"/>
                            </div>

                        <div class="form-group mb-md">                            
                            <div class="col-xs-12">
                                <label>Amount</label>
                                <input type="text" class="form-control" name="power_amount" id="power_amount"
                                       placeholder="" required>
                            </div>
                        </div>

                        <div class="form-group mb-md">                            
                            <div class="col-xs-12">
                                <label>Charges</label>
                                <input type="text" class="form-control" name="charge" id="charge" value="0"
                                       placeholder="" required>
                            </div>
                        </div>

                        <input type="hidden" name="order_id" id="order_id" value="<?php echo  $order_id; ?>" />
                        
     
                    <div class="panel-footer">
                        <div class="clearfix">
                        <input type="button" id="netplus-pay" value="Check Out" class="btn btn-success btn-raised pull-right"/>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- .container-fluid -->   
    </div>
    <!-- #page-content -->
</div>

<script src="https://netpluspay.com/inlinepayment/js/inline.js"></script>
<!-- <script src="https://netpluspay.com/inlinetestpayment/js/inline.js"></script> -->

<script style="text/javascript">
    $(document).ready(function () {
        $('#netplus-pay').hide();
        $('#verify').on('click', function (e) {
		    e.preventDefault();
            var user = $('#select1').val();
            $.ajax({
                type: "GET",
                url: "/power/verify_meter",
                data: {"user": user},
                success: function(data) {

                  var value = JSON.parse(data);
                  if(value.code == 00){
                        $('#netplus-pay').show();
                  }else{
                        $('#netplus-pay').hide();
                        window.location = '<?php echo base_url('power/create') ?>';
                  }
                
                }
            });

        });


        $("#netplus-pay").click(function (e) {
            e.preventDefault();
            var user = $('#select1').val();
            var charge = $('#charge').val();
            var amount = $('#power_amount').val();
            var order_id = $('#order_id').val();



            // Ajax call to initailize transaction
            $.ajax({
                type: "POST",
                url: "/power/saveTransaction",
                data: {"user": user, "charge": charge, "amount":amount, "order_id": order_id},
                success: function(data) {
                    var value = JSON.parse(data);
                    console.log(value);
                    if(value.code == 00){
                        e.preventDefault();
                        netpluspayPOP.setup(
                            {
                                //merchant: 'TEST5a81735b2a429',
                                merchant: 'MID5c1b6ca3f183d4.61130172',
                                customer_name: '<?php echo $this->aauth->get_user()->full_name; ?>',
                                email: '<?php echo $this->aauth->get_user()->email; ?>',
                                amount: amount,
                                currency_code: 'NGN',
                                narration: '<?php echo $narration; ?>',
                                order_id: order_id,
                                container: "paymentFrame",
                                onClose: function () {
                                    this.closeIframe();
                                   window.location = window.location.origin;
                                    console.log('window closed');
                                },
                                callBack: function (resp) {
                                    this.closeIframe();
                                    window.location = '<?php echo base_url('power/payment_response') ?>?' + $.param(resp)
                                }
                               
                                
                            }
                        );
                        netpluspayPOP.prepareFrame();
                    }else{
                            
                    }
                }
            });

        });
    });


</script>