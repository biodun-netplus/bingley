<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')): ?>
            <span class="pull-right" style="padding: 10px;">Meter No :
                <?= $this->aauth->get_user()->meter_no ?>
  </span>
        <?php endif; ?>
        <div class="page-heading">
            <h1>Cart</h1>

            <div class="options"></div>
        </div>
        <div class="container-fluid">
            <?php $this->load->view('includes/notification'); ?>
            <div data-widget-group="group1">
                <?php if (!empty($this->session->flashdata('flashMsg'))) { ?>
                    <div class="alert alert-danger">
                        <button class="close" data-close="alert"></button>
                        <span> <?php echo $this->session->flashdata('flashMsg') ?></span></div>
                <?php } ?>

            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2>Cart Details</h2>

                                <div class="panel-ctrls"></div>
                            </div>
                           
                            <form action="<?php echo site_url('shopping/checkout') ?>" method="post">
                                <div class="panel-body no-padding">
                                    <?php if($records[0]->product_type == 'PRODUCT'): ?>
                                    <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Service Name</th>
                                            <?php if ($records[0]->product_type == 'Service'): ?>
                                                <th>Number Of Months</th>
                                            <?php endif; ?>
                                            <th>Base Charge</th>
                                            <th>Take Or Pay</th>
                                            <th>Meter Number</th>
                                            <th>Total</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $TotalData = array();
                                        $i = 1;

                                        if (isset($records) && count($records) > 0) {
                                            foreach ($records as $LoopRecord) {
                                                $TotalData[] = $LoopRecord->product_price
                                                ?>
                                                <tr>
                                                    <td> <?php echo $LoopRecord->product_name ?></td>
                                                    <?php if ($LoopRecord->product_type == 'Service'): ?>
                                                        <td>
                                                            <select name="product_quantity[]" id="">
                                                                <?php for ($i = 1; $i <= 12; $i++) { ?>
                                                                    <option <?php if ($LoopRecord->quantity == $i) { ?> selected <?php } ?>
                                                                        value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                                <?php } ?>

                                                            </select></td>
                                                    <?php else: ?>
                                                        <input type="hidden" name="product_quantity" value="1"/>
                                                    <?php endif; ?>
                                                    <td id="base_charge_td_<?php echo $LoopRecord->id; ?>">
                                                        <span
                                                            id="base_charge_val_<?php echo $LoopRecord->id; ?>"><?php echo $LoopRecord->base_charge; ?> </span>
                                                        <br>
                                                        <?php if ($LoopRecord->product_type == 'Power') { ?>
                                                            <?php /*?> <a class="btn btn-danger btn-raised base_charge" href="javascript:;" id="<?php echo $LoopRecord->id ; ?>" >Remove</a><?php */ ?>
                                                        <?php } ?>
                                                    </td>
                                                    <td id="take_or_pay_td_<?php echo $LoopRecord->id; ?>">

                                                        <span
                                                            id="take_or_pay_val_<?php echo $LoopRecord->id; ?>"> <?php echo $LoopRecord->take_or_pay ?></span>
                                                        <br>
                                                        <?php if ($LoopRecord->product_type == 'Power') { ?>
                                                            <?php /*?> <a class="btn btn-danger btn-raised take_or_pay" id="<?php echo $LoopRecord->id ; ?>" href="javascript:;">Remove</a><?php */ ?>
                                                        <?php } ?>
                                                    </td>

                                                    <td> <?php echo $c_user->meter_no ?></td>
                                                    <td>
                                                        <?php
                                                        if ($LoopRecord->product_type == 'Power'){

                                                            ?>

                                                            <input type="hidden" name="product_id[]"
                                                                   id="input_product_id_<?php echo $LoopRecord->id; ?>"
                                                                   value="<?php echo $LoopRecord->id; ?>">
                                                            <input type="hidden" name="product_price[]"
                                                                   id="input_product_price_<?php echo $LoopRecord->id; ?>"
                                                                   value="<?php echo $LoopRecord->price; ?>">
                                                            <input type="hidden" name="product_price_type[]"
                                                                   id="input_product_price_type_<?php echo $LoopRecord->id; ?>"
                                                                   value="<?php echo $LoopRecord->product_price_type; ?>">
                                                            <span id="product_price_<?php echo $LoopRecord->id; ?>">
					  <?php echo $LoopRecord->price; ?>
                       
                      </span>
                                                        <?php }
                                                        else if ($LoopRecord->product_type == 'Top Up'){ ?>


                                                            <input type="hidden" name="product_id[]"
                                                                   id="input_product_id_<?php echo $LoopRecord->id; ?>"
                                                                   value="<?php echo $LoopRecord->id; ?>">
                                                            <input type="text" name="product_price[]"
                                                                   id="input_product_price_<?php echo $LoopRecord->id; ?>"
                                                                   value="<?php echo $LoopRecord->price; ?>">
                                                            <input type="hidden" name="product_price_type[]"
                                                                   id="input_product_price_type_<?php echo $LoopRecord->id; ?>"
                                                                   value="<?php echo $LoopRecord->product_price_type; ?>">
                                                            &nbsp; &nbsp; <a
                                                                href="<?php echo base_url('shopping/cart/delete/' . $LoopRecord->cart_id); ?>"><span
                                                                    class="icon"><i
                                                                        class="material-icons">delete</i></span></a>
                                                        <?php }
                                                        else{
                                                        ?>
                                                        <input type="hidden" name="product_id[]"
                                                               id="input_product_id_<?php echo $LoopRecord->id; ?>"
                                                               value="<?php echo $LoopRecord->id; ?>">
                                                        <input type="hidden" name="product_price[]"
                                                               id="input_product_price_<?php echo $LoopRecord->id; ?>"
                                                               value="<?php echo $LoopRecord->price; ?>">
                                                        <input type="hidden" name="product_price_type[]"
                                                               id="input_product_price_type_<?php echo $LoopRecord->id; ?>"
                                                               value="<?php echo $LoopRecord->product_price_type; ?>">
                      <span id="product_price_<?php echo $LoopRecord->id; ?>">
					  <?php echo $LoopRecord->price; ?>


                      <?php } ?>
                                                    </td>
                                                </tr>
                                                <?php $i++;
                                            }
                                        } ?>


                                        </tbody>
                                    </table>
                                    <?php endif; ?>
                                </div>
                                <div class="panel-footer">

                                    <input type="submit" name="check_out" value="Pay with Account"
                                           class="btn btn-success btn-raised pull-right"/>
                                    
                                    <input type="submit" name="check_out" value="Pay with Card"
                                           class="btn btn-primary btn-raised pull-right"/>
                                </div>

                            </form>
                            <div class="col-xs-12 pull-right" align="right">
                                <form action="<?php echo site_url('shopping/couponpay') ?>" method="post">
                                    <input type="text" name="coupon_code" required placeholder="Use Coupon Code"
                                           class="form-control" style="display: inline-block;width: auto;">
                                    <input type="submit" name="pay_with_coupon" value="Pay with Coupon"
                                           class="btn btn-info btn-raised">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>
