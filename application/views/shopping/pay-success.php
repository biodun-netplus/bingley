<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>PAYMENT SUCCESSFUL</title>
</head>
<body bgcolor="#eee">

<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#fff">
  <!--DWLayoutTable-->
  <tr>
    <td height="345" colspan="6" valign="top"><img src="http://willcoonline.com.ng/bingley/assets/image/banner.jpg" height="345" width="600" border="none" /></td>
  </tr>
  <tr>
    <td width="35" height="25">&nbsp;</td>
    <td width="101">&nbsp;</td>
    <td width="161">&nbsp;</td>
    <td width="201">&nbsp;</td>
    <td width="70">&nbsp;</td>
    <td width="32">&nbsp;</td>
  </tr>
  <tr>
    <td height="22">&nbsp;</td>
    <td colspan="2" valign="top" style="font-size:16px; font-family:Verdana, Arial, Helvetica, sans-serif; font-weight:bold;   ">PAYMENT SUCCESSFUL</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="21"></td>
    <td valign="top" style="border-top:2px solid #1DE154;"><!--DWLayoutEmptyCell-->&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="3"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="18"></td>
    <td colspan="2" valign="top" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:14px; font-weight:200;">Hello <?php echo $full_name ?>,</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="12"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="66"></td>
    <td colspan="3" valign="top" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; line-height:0.6cm; font-weight:300;">Your <?php echo $product_name; ?> payment was successful. <br />
      <br />
    Please see your transaction details below:</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="20"></td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="160"></td>
    <td colspan="4" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#464646;">
      <!--DWLayoutTable-->
      <tr>
        <td width="199" height="40" valign="middle" style="padding:10px 10px 10px 20px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; color:#FFFFFF; border-bottom: 1px solid #fff; border-right: 1px solid #fff; ">TRASACTION DATE </td>
          <td width="334" valign="middle" style="padding:10px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; color:#fff; border-bottom: 1px solid #fff;"><?php echo $tr_date ?>   	<?php echo $tr_time ?></td>
        </tr>
      <tr>
        <td height="40" valign="middle" style="padding:10px 10px 10px 20px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#fff; font-weight:bold; border-right: 1px solid #fff; border-bottom: 1px solid #fff; ">TRANSACTION ID </td>
          <td valign="middle" style="padding:10px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; color: #fff; border-bottom: 1px solid #fff;  "><?php echo $order_id; ?></td>
        </tr>
      <tr>
        <td height="40" valign="middle" style="padding:10px 10px 10px 20px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; color:#FFFFFF; border-bottom: 1px solid #fff; border-right: 1px solid #fff; ">TRANSACTION AMOUNT</td>
          <td valign="middle" style="padding:10px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; color: #fff; border-bottom: 1px solid #fff;  ">N<?php echo number_format($amount_paid); ?></td>
        </tr>
      <tr>
        <td height="40" valign="middle" style="padding:10px 10px 10px 20px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; color:#FFFFFF; border-right: 1px solid #fff; ">DESCRIPTION</td>
          <td valign="middle" style="padding:10px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; color: #fff; "><?php echo $product_name; ?></td>
      </tr>
      
      <?php if($is_power == 1): ?>
        <tr>
          <td height="40" valign="middle" style="padding:10px 10px 10px 20px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#fff; font-weight:bold; border-right: 1px solid #fff; border-bottom: 1px solid #fff; ">STATUS</td>
            <td valign="middle" style="padding:10px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; color: #fff; border-bottom: 1px solid #fff;  "><?php echo $token_desc; ?></td>
          </tr>
        <tr>
        <tr>
          <td height="40" valign="middle" style="padding:10px 10px 10px 20px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#fff; font-weight:bold; border-right: 1px solid #fff; border-bottom: 1px solid #fff; ">RECIEPT ID</td>
            <td valign="middle" style="padding:10px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; color: #fff; border-bottom: 1px solid #fff;  "><?php echo $token_no; ?></td>
          </tr>
        <tr>
        <tr>
          <td height="40" valign="middle" style="padding:10px 10px 10px 20px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; color:#fff; font-weight:bold; border-right: 1px solid #fff; border-bottom: 1px solid #fff; ">TOKEN UNIT</td>
            <td valign="middle" style="padding:10px; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; color: #fff; border-bottom: 1px solid #fff;  "><?php echo $token_amount; ?></td>
          </tr>
        <tr>
      <?php endif; ?>
      
      
    </table></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="19"></td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="44"></td>
    <td colspan="3" valign="top" style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:13px; line-height:0.6cm; font-weight:300; ">For further information, please call <a href="tel:+2347032701415" style="text-decoration:none; color:#000; "></a> or send an email to <a href="mailto:#" style="text-decoration:none; color:#000;"></a></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="12"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="52" colspan="6" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#E6E4E7">
      <!--DWLayoutTable-->
      <tr>
        <td width="368" height="52">&nbsp;</td>
          <td width="206" valign="middle"><img src="https://netpluspay.com/images/netpluspayLogoGreen.png" width="206" height="31" border="none"/></td>
          <td width="26">&nbsp;</td>
        </tr>
      
      
    </table></td>
  </tr>
  </table>
</body>
</html>
