<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')): ?>
            <span class="pull-right" style="padding: 10px;">Meter No :
                <?= $this->aauth->get_user()->meter_no ?>
  </span>
        <?php endif; ?>
        <div class="page-heading">
            <h1>Cart</h1>

            <div class="options"></div>
        </div>
        <div class="container-fluid">
            <div data-widget-group="group1">
                <?php if (!empty($this->session->flashdata('flashMsg'))) { ?>
                    <div class="alert alert-danger">
                        <button class="close" data-close="alert"></button>
                        <span> <?php echo $this->session->flashdata('flashMsg') ?></span></div>
                <?php } ?>

            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2>Cart Details</h2>

                                <div class="panel-ctrls"></div>
                            </div>

                            <div class="panel-body no-padding">
                                <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                            <th>Service Name</th>
                                            <th>Payment Type</th>
                                            <th>Meter Number</th>
                                            <th>Number Of Months</th>
                                            <th>Price</th>


                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $TotalData = array();
                                    $i = 1;
                                    if (isset($records) && count($records) > 0) {
                                        foreach ($records as $LoopRecord) {
                                            $TotalData[] = $LoopRecord->price;

                                            ?>
                                            <tr>
                                                <td> <?php echo $LoopRecord->product_name ?></td>
                                                <td> <?php echo $LoopRecord->product_price_type ?></td>
                                                <td> <?php echo $LoopRecord->meter_no ?></td>
                                                <td> <?php echo $LoopRecord->quantity ?></td>
                                                <td> &#x20A6; <?php echo $LoopRecord->price ?></td>


                                            </tr>
                                            <?php $i++;
                                        }
                                    } ?>

                                    <tr>
                                        <!-- <td colspan="4"> <?= $charges; ?>% processing fee</td>
                                        <td>
                                            &#x20A6; <?php $pfee = (array_sum($TotalData) / 100) * $charges ?> <?php if ($pfee > 2000) {
                                                $pfee = 2000;
                                            } ?> <?= $pfee ?></td> -->

                                    <!-- <tr>
                                        <td colspan="4"> convenience fee</td>
                                        <td>  &#x20A6; 100</td>

                                    </tr> -->

                                    <tr>
                                        <td colspan="4"> Total</td>
                                        <td>  &#x20A6; <?php echo $pfee  + array_sum($TotalData) ?></td>
                                        <!-- <td>  &#x20A6; <?php echo array_sum($TotalData) ?></td> -->

                                    </tr>
                                    <tr>
                                        <td colspan="5">
                                            <form name="submit2fidelity_form" id="submit2fidelity_form"
                                                  action="https://netpluspay.com/payment/paysrc/" target="_self"
                                                  method="post">
                                                <input type="hidden" name="full_name"
                                                       value="<?php echo $this->aauth->get_user()->full_name; ?>"/>
                                                <input type="hidden" name="total_amount"
                                                       value="<?php echo $amount; ?>"/>
                                                <input type="hidden" name="narration"
                                                       value="<?php echo $narration; ?>"/>
                                                <input type="hidden" name="email"
                                                       value="<?php echo $this->aauth->get_user()->email; ?>"/>
                                                <input type="hidden" name="order_id" value="<?php echo $order_id; ?>"/>
                                                <input type="hidden" name="merchant_id"
                                                       value="MID5a3b9f942b3a56.21053140"/>
                                                <input type="hidden" name="return_url"
                                                       value="<?php echo base_url('shopping/callback'); ?>"/>
                                                <input type="hidden" name="currency_code" value="NGN"/>
                                                <input type="button" id="netplus-pay" value="Check Out"
                                                       class="btn btn-success btn-raised pull-right"/>
                                            </form>
                                        </td>


                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="panel-heading">
                                <!-- <h4>NB: For Account based payment you have 4% decrease</h4> -->
                                <div class="panel-ctrls"></div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>
<script src="https://netpluspay.com/inlinepayment/js/inline.js"></script>
<!-- <script src="https://netpluspay.com/inlinetestpayment/js/inline.js"></script> -->

<script style="text/javascript">
    $(document).ready(function () {
        $("#netplus-pay").click(function (e) {
            e.preventDefault();
            
            netpluspayPOP.setup(
                {
                    //merchant: 'TEST5a81735b2a429',
                    merchant: 'MID5c8f59fdb9e920.15604128',
                    customer_name: '<?php echo $this->aauth->get_user()->full_name; ?>',
                    email: '<?php echo $this->aauth->get_user()->email; ?>',
                    amount: '<?php echo $amount; ?>',
                    currency_code: 'NGN',
                    narration: '<?php echo $narration; ?>',
                    order_id: '<?php echo $order_id; ?>',
                    container: "paymentFrame",
                    callBack: function (resp) {
                        console.log(resp)
                        this.closeIframe();
                        //window.location = '<?php echo base_url('shopping/payment_response') ?>?code='+resp.code+'&trans_id='+resp.trans_id+'&order_id='+resp.orderid
                        window.location = '<?php echo base_url('shopping/payment_response') ?>?' + $.param(resp)
                        //console.log($.param(resp));
                    },
                    onClose: function () {
                        window.location = 'http://willcoonline.com.ng/bingley/';
                        console.log('window closed');
                    }
                }
            );
            netpluspayPOP.prepareFrame();

        });
    });


</script>

<!--<body onload="document.submit2fidelity_form.submit()">
<form name="submit2fidelity_form" action="https://netpluspay.com/testpayment/paysrc/" target="_self" method="post">
	<input type="hidden" name="full_name" value="<?php echo $this->aauth->get_user()->full_name; ?>" />
    <input type="hidden" name="total_amount" value="<?php echo $amount; ?>" />
    <input type="hidden" name="narration" value="<?php echo $narration; ?>" />
    <input type="hidden" name="email" value="<?php echo $this->aauth->get_user()->email; ?>" />
    <input type="hidden" name="order_id" value="<?php echo $order_id; ?>" />
    <input type="hidden" name="merchant_id" value="TEST57761f53d9ca8" />
    <input type="hidden" name="return_url" value="<?php echo base_url('shopping/callback'); ?>" />
    <input type="hidden" name="currency_code" value="NGN" />
</form>
</body>
-->