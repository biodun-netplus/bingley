<div class="static-content">
    <div class="page-content">
        <?php if ($this->aauth->is_member('Merchant')): ?>
            <span class="pull-right" style="padding: 10px;">Meter No :
                <?= $this->aauth->get_user()->meter_no ?>
  </span>
        <?php endif; ?>
        <div class="page-heading">
            <h1>Cart</h1>

            <div class="options"></div>
        </div>
        <div class="container-fluid">
            <?php $this->load->view('includes/notification'); ?>
            <div data-widget-group="group1">
                <?php if (!empty($this->session->flashdata('flashMsg'))) { ?>
                    <div class="alert alert-danger">
                        <button class="close" data-close="alert"></button>
                        <span> <?php echo $this->session->flashdata('flashMsg') ?></span></div>
                <?php } ?>

            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2>Cart Details</h2>

                                <div class="panel-ctrls"></div>
                            </div>
                            <form action="<?php echo site_url('shopping/checkout') ?>" method="post">
                                <div class="panel-body no-padding">
                                        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Bill Name</th>
                                                <th>Meter No</th>
                                                <th>Amount</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php $TotalData = array(); foreach($records as $bill): $TotalData[] = $bill->amount; ?>
                                                <tr>
                                                    <td><?php echo $bill->description ?></td>
                                                    <td><?php echo $c_user->meter_no ?></td>
                                                    <td>&#x20A6; <?php echo number_format($bill->amount); ?></td>
                                                    <input type="hidden" name="product_id[]"
                                                           id="input_product_id_<?php echo $bill->id; ?>"
                                                           value="<?php echo $bill->id; ?>">
                                                    <input type="hidden" name="product_price[]"
                                                           id="input_product_price_<?php echo $bill->id; ?>"
                                                           value="<?php echo $bill->amount; ?>">
                                                    <input type="hidden" name="product_quantity[]"
                                                           id="input_product_price_<?php echo $bill->id; ?>"
                                                           value="1">
                                                    <input type="hidden" name="product_price_type[]"
                                                           id="input_product_price_type_<?php echo $bill->id; ?>"
                                                           value="<?php echo $bill->product_price_type; ?>">
                                                </tr>

                                            <?php endforeach; ?>


                                            </tbody>
                                        </table>
                                </div>
                                <div class="panel-footer">

                                    <input type="submit" name="check_out" value="Check Out"
                                           class="btn btn-success btn-raised pull-right"/>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- .container-fluid -->
    </div>
    <!-- #page-content -->
</div>
