<!DOCTYPE html>
<html>

<head>
    <title><?php echo $title; ?> -Bingley Apartments</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">

    <!-- <link rel="shortcut icon" href="http://willcoonline.com.ng/bingley/assets/assets/img/logo-icon-dark.png"> -->

    <link type='text/css' href='http://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500' rel='stylesheet'>
    <link type='text/css' href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="http://willcoonline.com.ng/bingley/assets/assets/fonts/font-awesome/css/font-awesome.min.css" type="text/css"
          rel="stylesheet">
    <!-- Font Awesome -->
    <link href="http://willcoonline.com.ng/bingley/assets/assets/css/styles.css" type="text/css" rel="stylesheet">
    <!-- Core CSS with all styles -->
    <link href="http://willcoonline.com.ng/bingley/assets/assets/plugins/codeprettifier/prettify.css" type="text/css"
          rel="stylesheet">
    <!-- Code Prettifier -->

    <link href="http://willcoonline.com.ng/bingley/assets/assets/plugins/dropdown.js/jquery.dropdown.css" type="text/css"
          rel="stylesheet">
    <!-- iCheck -->
    <link href="http://willcoonline.com.ng/bingley/assets/assets/plugins/progress-skylo/skylo.css" type="text/css"
          rel="stylesheet">
    <!-- Skylo -->

    <!--[if lt IE 10]>
    <script src="http://willcoonline.com.ng/bingley/assets/assets/js/media.match.min.js"></script>
    <script src="http://willcoonline.com.ng/bingley/assets/assets/js/respond.min.js"></script>
    <script src="http://willcoonline.com.ng/bingley/assets/assets/js/placeholder.min.js"></script>
    <![endif]-->
    <!-- The following CSS are included as plugins and can be removed if unused-->


    <link href="http://willcoonline.com.ng/bingley/assets/assets/plugins/fullcalendar/fullcalendar.css" type="text/css"
          rel="stylesheet">
    <!-- FullCalendar -->
    <link href="http://willcoonline.com.ng/bingley/assets/assets/plugins/jvectormap/jquery-jvectormap-2.0.2.css" type="text/css"
          rel="stylesheet">
    <link href="http://willcoonline.com.ng/bingley/assets/assets/less/card.less" type="text/css" rel="stylesheet">

    <link href="http://willcoonline.com.ng/bingley/assets/assets/plugins/chartist/dist/chartist.min.css" type="text/css"
          rel="stylesheet">
    <link href="http://willcoonline.com.ng/bingley/assets/assets/plugins/form-select2/select2.css" type="text/css"
          rel="stylesheet">
    <link href="http://willcoonline.com.ng/bingley/assets/assets/plugins/form-daterangepicker/daterangepicker-bs3.css"
          type="text/css" rel="stylesheet">
    <!-- DateRangePicker -->
    <!-- chartist -->
    <style>
        div#defaultTable_paginate {
            float: right;
        }
    </style>
    <script src="http://willcoonline.com.ng/bingley/assets/assets/js/jquery-1.10.2.min.js"></script>
    <!-- Load jQuery -->

</head>

<body class="animated-content infobar-overlay">


<header id="topnav" class="navbar navbar-default navbar-fixed-top navbar-default" role="banner">
    <!-- <div id="page-progress-loader" class="show"></div> -->

    <div class="logo-area">
        <a class="navbar-brand navbar-brand-primary" href="<?php echo base_url(); ?>">
            <h3 style="color:white;text-align:center;">Bingley</h3>
            <!--
            <img class="show-on-collapse img-logo-white" alt="Paper" src="assets/img/logo-icon-white.png">
            <img class="show-on-collapse img-logo-dark" alt="Paper" src="assets/img/logo-icon-dark.png">
            <img class="img-white" alt="Paper" src="assets/img/logo-white.png">
            <img class="img-dark" alt="Paper" src="assets/img/logo-dark.png">
-->
        </a>
        
            <span id="trigger-sidebar" class="toolbar-trigger toolbar-icon-bg stay-on-search">
			<a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar">
				<span class="icon-bg">
					<i class="material-icons">menu</i>
				</span>
            </a>
            </span>
        <!--
        <span id="trigger-search" class="toolbar-trigger toolbar-icon-bg ov-h">
        <a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar">
            <span class="icon-bg">
                <i class="material-icons">search</i>
            </span>
        </a>
        </span>

    <div id="search-box">
        <input class="form-control" type="text" placeholder="Search..." id="search-input"></input>
    </div>
    -->
    </div>
    <!-- logo-area -->

    <ul class="nav navbar-nav toolbar pull-right">

        <li class="toolbar-icon-bg appear-on-search ov-h" id="trigger-search-close">
            <a class="toggle-fullscreen"><span class="icon-bg">
	        	<i class="material-icons">close</i>
	        </span></i></a>
        </li>
        <li class="toolbar-icon-bg" id="trigger-fullscreen">
            <a href="#" class="toggle-fullscreen"><span class="icon-bg">
	        	<i class="material-icons">fullscreen</i>
	        </span></i></a>
        </li>
        <li class="toolbar-icon-bg">
            <a href="<?php echo site_url('dashboard/logout'); ?>" class=""><span class="icon-bg">
	        	<i class="material-icons" data-toggle="tooltip" data-placement="bottom" title="Logout">lock_open</i>
	        </span></i></a>
        </li>


    </ul>

</header>

<div id="wrapper">
    <div id="layout-static">
        <div class="static-sidebar-wrapper sidebar-default">
            <div class="static-sidebar">
                <div class="sidebar">
                    <div class="widget" id="widget-profileinfo">
                        <div class="widget-body">
                            <div class="userinfo ">
                                <div class="avatar pull-left">
                                    <img src="http://willcoonline.com.ng/bingley/assets/assets/img/profile.png"
                                         class="img-responsive img-circle">
                                </div>
                                <div class="info">
                                    <span class="username"><?= $this->aauth->get_user()->full_name ?></span>
                                    <span class="useremail"><?= $this->aauth->get_user()->email ?></span>
                                </div>

                                <div class="acct-dropdown clearfix dropdown">
                                    <span class="pull-left"><span class="online-status online"></span>Online</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="widget stay-on-collapse" id="widget-sidebar">
                        <nav role="navigation" class="widget-body">
                            <ul class="acc-menu">
                                <li class="nav-separator"><span>Navigation</span></li>

                                <li><a class="withripple" href="<?php echo site_url('dashboard'); ?>"><span
                                            class="icon"><i
                                                class="material-icons">home</i></span><span>Dashboard</span></a></li>
                                <li><a class="withripple" href="javascript:;"><span class="icon"><i
                                                class="material-icons">settings</i></span><span>Profile</span></a>
                                    <ul class="acc-menu">
                                        <li><a class="withripple" href="<?php echo site_url('profile/settings') ?>">Settings</a>
                                        </li>
                                    </ul>
                                </li>


                                <?php if ($this->aauth->is_member('Admin')): ?>
                                <li><a class="withripple" href="javascript:;"><span class="icon"><i
                                                class="fa fa-history"></i></span><span>Payment History</span></a>
                                    <ul class="acc-menu">
                                        <!-- <li><a class="withripple" href="<?php echo site_url('payment/list') ?>">List
                                                of Power Payments</a></li>
                                        <li><a class="withripple" href="<?php echo site_url('shopping/topuppayment') ?>">List of Electricity Token
                                                payments</a></li>
                                        <li><a class="withripple" href="<?php echo site_url('shopping/adminpowerpayments') ?>">Admin Power
                                                payments</a></li> -->
                                        <li><a class="withripple"
                                               href="<?php echo site_url('shopping/shoppinglist') ?>">List of
                                                Service Payments</a></li>
<!--                                        
                                        <li><a class="withripple"
                                               href="<?php echo site_url('shopping/waterpumppayment') ?>">List of water pump
                                                payments</a></li> -->
                                        <!-- <li><a class="withripple"
                                               href="<?php echo site_url('shopping/securitychargepayment') ?>">List of security charge
                                                payments</a></li>
                                        <li><a class="withripple"
                                               href="<?php echo site_url('shopping/otherpayments') ?>">List of other
                                                payments</a></li> -->
                                        <!-- <li><a class="withripple"
                                               href="<?php echo site_url('shopping/servicechargepayment') ?>">List of service charge
                                                payments</a></li> -->
                                        <li><a class="withripple"
                                               href="<?php echo site_url('shopping/servicechargedebt') ?>">Credit Service Charge</a></li>
                                        <li><a class="withripple"
                                               href="<?php echo site_url('payment/outstanding_list') ?>">Outstanding Bill
                                                Payments</a></li>
                                    </ul>
                                </li>
                                <li><a class="withripple" href="<?php echo site_url('users/userguide'); ?>"><span
                                            class="icon"><i
                                                class="material-icons">user_guide</i></span><span>Users Guide</span>
                                    </a>

                                </li>

                                <?php endif; ?>

                                <?php if ($this->aauth->is_member('Public')): ?>
                                <li><a class="withripple" href="javascript:;"><span class="icon"><i
                                                class="fa fa-history"></i></span><span>Payment History</span></a>
                                    <ul class="acc-menu">
                                        <!-- <li><a class="withripple" href="<?php echo site_url('payment/list') ?>">List
                                                of Power Payments</a></li> -->
                                        <!-- <li><a class="withripple"
                                               href="<?php echo site_url('shopping/topuppayment') ?>">List of Electricity Token
                                                payments</a></li> -->
                                        <!-- <li><a class="withripple" href="<?php echo site_url('shopping/adminpowerpayments') ?>">Admin Power
                                                payments</a></li> -->
                                        <li><a class="withripple"
                                               href="<?php echo site_url('shopping/shoppinglist') ?>">List of
                                                Service Payments</a></li>
                                        <!-- <li><a class="withripple"
                                               href="<?php echo site_url('shopping/waterpumppayment') ?>">List of water pump
                                                payments</a></li> -->
<!--                                   
                                        <li><a class="withripple"
                                               href="<?php echo site_url('shopping/securitychargepayment') ?>">List of security charge
                                                payments</a></li> -->
                                        <!-- <li><a class="withripple"
                                               href="<?php echo site_url('shopping/otherpayments') ?>">List of other
                                                payments</a></li> -->

                                        <li><a class="withripple"
                                               href="<?php echo site_url('payment/outstanding_list') ?>">Outstanding Bill
                                                Payments</a></li>

                                    </ul>
                                </li>
                                <li><a class="withripple" href="<?php echo site_url('product/userguide'); ?>"><span
                                            class="icon"><i
                                                class="material-icons">user_guide</i></span><span>Users Guide</span>
                                    </a>

                                </li>
                                <?php endif; ?>

                                <?php if ($this->aauth->is_member('Merchant')): ?>
                                <li><a class="withripple" href="javascript:;"><span class="icon"><i
                                                class="fa fa-history"></i></span><span>Payment History</span></a>
                                    <ul class="acc-menu">
                                        <li><a class="withripple"
                                               href="<?php echo site_url('payment/list') ?>">List</a></li>
                                        <li><a class="withripple"
                                               href="<?php echo site_url('payment/pay') ?>">New</a></li>
                                    </ul>
                                </li>
                                <?php endif; ?>


                                <?php
                                $AnnouncementCount = $this->db->query("select * from announcement")->result();

                                ?>

                                <?php if ($this->aauth->is_member('Public')):?>
                                <li><a class="withripple" href="<?php echo site_url('product/product'); ?>"><span
                                            class="icon"><i
                                                class="material-icons">account_balance_wallet</i></span><span>Bill Payments</span></a>

                                </li>

                                <li><a class="withripple" href="<?php echo site_url('product/announcement'); ?>"><span
                                            class="icon"><i
                                                class="material-icons">announcement</i></span><span>Announcement</span>
                                        <span
                                            class="badge badge-primary"><?php echo count($AnnouncementCount); ?></span>
                                    </a>

                                </li>
                                <?php endif; ?>
                                <?php if ($this->aauth->is_member('Admin')):?>
                                <li><a class="withripple" href="<?php echo site_url('coupon'); ?>"><span class="icon"><i
                                                class="material-icons">account_balance_wallet</i></span><span>Coupons</span></a>
                                <li><a class="withripple" href="<?php echo site_url('outstanding'); ?>"><span class="icon"><i
                                                class="material-icons">account_balance_wallet</i></span><span>Outstanding Bill</span></a>

                                </li>
                                <li><a class="withripple" href="<?php echo site_url('users/regulardirectory'); ?>"><span
                                            class="icon"><i
                                                class="material-icons">account_balance_wallet</i></span><span>Directory</span></a>

                                </li>

                                <?php endif; ?>
                                <?php if ($this->aauth->is_member('Report')):?>
                                <li><a class="withripple" href="<?php echo site_url('users/regulardirectory'); ?>"><span
                                            class="icon"><i
                                                class="material-icons">account_balance_wallet</i></span><span>Directory</span></a>

                                </li>
                                <li><a class="withripple" href="<?php echo site_url('report'); ?>"><span class="icon"><i
                                                class="material-icons">account_balance_wallet</i></span><span>Report</span></a>
                                    <?php endif; ?>

                                    <?php if ($this->aauth->is_member('Admin')): ?>

                                <li><a class="withripple" href="<?php echo site_url('product/announcement'); ?>"><span
                                            class="icon"><i
                                                class="material-icons">announcement</i></span><span>Announcement</span>
                                        <span
                                            class="badge badge-primary"><?php echo count($AnnouncementCount); ?></span>

                                    </a>

                                </li>
                                <!-- <li><a class="withripple" href="<?php echo site_url('product/meter_tariff'); ?>"><span
                                            class="icon"><i
                                                class="material-icons">account_balance_wallet</i></span><span>Meter Tariff</span></a>

                                </li> -->
                                <!-- <li><a class="withripple" href="<?php echo site_url('power'); ?>"><span
                                            class="icon"><i
                                                class="material-icons">account_balance_wallet</i></span><span>Vend Power</span></a>
                                </li> -->

                                <li><a class="withripple" href="<?php echo site_url('product'); ?>"><span
                                            class="icon"><i
                                                class="material-icons">account_balance_wallet</i></span><span>Bills</span></a>

                                </li>
                                <li><a class="withripple" href="<?php echo site_url('report'); ?>"><span class="icon"><i
                                                class="material-icons">account_balance_wallet</i></span><span>Report</span></a>

                                </li>
                                <li><a class="withripple" href="javascript:;"><span class="icon"><i
                                                class="material-icons">account_box</i></span><span>Users</span></a>
                                    <ul class="acc-menu">
                                        <li><a class="withripple" href="<?php echo site_url('users/admin') ?>">Admin</a>
                                        </li>

                                        <li><a class="withripple"
                                               href="<?php echo site_url('users/regular_user/add') ?>">Create Resident
                                            </a></li>

                                        <li><a class="withripple" href="<?php echo site_url('users/register_user') ?>">View
                                                Registered Users</a></li>

                                    </ul>
                                </li>

                                </li>


                                <?php endif; ?>

                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="static-content-wrapper">

            <?= $body ?>
            <footer role="contentinfo">
                <div class="clearfix">
                    <ul class="list-unstyled list-inline pull-left">
                        <li>
                            <h6 style="margin: 0;">&copy; <?php echo date('Y'); ?></h6>
                        </li>
                    </ul>
                </div>
            </footer>

        </div>

    </div>
</div>
<div id="paymentFrame"></div>

<script src="http://willcoonline.com.ng/bingley/assets/assets/js/jqueryui-1.10.3.min.js"></script>
<!-- Load jQueryUI -->
<script src="http://willcoonline.com.ng/bingley/assets/assets/js/bootstrap.min.js"></script>
<!-- Load Bootstrap -->
<script src="http://willcoonline.com.ng/bingley/assets/assets/js/enquire.min.js"></script>
<!-- Load Enquire -->

<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/velocityjs/velocity.min.js"></script>
<!-- Load Velocity for Animated Content -->
<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/velocityjs/velocity.ui.min.js"></script>

<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/progress-skylo/skylo.js"></script>
<!-- Skylo -->

<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/wijets/wijets.js"></script>
<!-- Wijet -->

<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/sparklines/jquery.sparklines.min.js"></script>
<!-- Sparkline -->

<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/codeprettifier/prettify.js"></script>
<!-- Code Prettifier  -->

<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js"></script>
<!-- Bootstrap Tabdrop -->

<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/nanoScroller/js/jquery.nanoscroller.min.js"></script>
<!-- nano scroller -->

<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/dropdown.js/jquery.dropdown.js"></script>
<!-- Fancy Dropdowns -->
<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/bootstrap-material-design/js/material.min.js"></script>
<!-- Bootstrap Material -->
<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/bootstrap-material-design/js/ripples.min.js"></script>
<!-- Bootstrap Material -->

<script src="http://willcoonline.com.ng/bingley/assets/assets/js/application.js"></script>
<script src="http://willcoonline.com.ng/bingley/assets/assets/demo/demo.js"></script>
<script src="http://willcoonline.com.ng/bingley/assets/assets/demo/demo-switcher.js"></script>

<!-- End loading site level scripts -->

<!-- Load page level scripts-->

<!-- Charts -->
<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/charts-flot/jquery.flot.min.js"></script>
<!-- Flot Main File -->
<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/charts-flot/jquery.flot.pie.min.js"></script>
<!-- Flot Pie Chart Plugin -->
<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/charts-flot/jquery.flot.stack.min.js"></script>
<!-- Flot Stacked Charts Plugin -->
<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/charts-flot/jquery.flot.resize.min.js"></script>
<!-- Flot Responsive -->
<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/charts-flot/jquery.flot.tooltip.min.js"></script>
<!-- Flot Tooltips -->
<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/charts-flot/jquery.flot.spline.js"></script>
<!-- Flot Curved Lines -->
<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/easypiechart/jquery.easypiechart.min.js"></script>
<!-- EasyPieChart-->
<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/curvedLines-master/curvedLines.js"></script>
<!-- marvinsplines -->

<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/form-daterangepicker/daterangepicker.js"></script>
<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/form-daterangepicker/moment.min.js"></script>
<!-- Moment.js for Date Range Picker -->

<!-- Date Range Picker -->
<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<!-- Datepicker -->
<!-- <script src="assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script> -->
<!-- DateTime Picker -->

<!-- <script src="assets/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>   -->
<!-- jVectorMap -->
<!-- <script src="assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>  -->
<!--World Map-->
<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/chartist/dist/chartist.min.js"></script>
<!-- chartist -->
<script src="http://willcoonline.com.ng/bingley/assets/assets/demo/demo-index.js"></script>

<!-- Initialize scripts for this page-->

<!-- End loading page level scripts-->


<script>
    // See Docs
    window.ParsleyConfig = {
        successClass: 'has-success'
        , errorClass: 'has-error'
        , errorElem: '<span></span>'
        , errorsWrapper: '<span class="help-block"></span>'
        , errorTemplate: "<div></div>"
        , classHandler: function (el) {
            return el.$element.closest(".form-group");
        }
    };
</script>
<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/form-parsley/parsley.js"></script>
<!-- Validate Plugin / Parsley -->
<script src="http://willcoonline.com.ng/bingley/assets/assets/demo/demo-formvalidation.js"></script>
<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/form-select2/select2.min.js"></script>
<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="http://willcoonline.com.ng/bingley/assets/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.base_charge').click(function () {
            var ProductId = this.id;
            var baseCharge = $('#base_charge_val_' + ProductId).text();
            var TakeorPay = $('#take_or_pay_val_' + ProductId).text();
            var TotalPrice = $('#product_price_' + ProductId).text();
            var ProductAmount = TotalPrice - baseCharge;
            $('#input_product_id_' + ProductId).val(ProductId);
            $('#input_product_price_' + ProductId).val(ProductAmount);
            $('#input_product_price_type_' + ProductId).val('Take Or Pay');
            $('#base_charge_td_' + ProductId).text('');
            $('#take_or_pay_td_' + ProductId).find('a.take_or_pay').remove();
            ;
            $('#product_price_' + ProductId).text(ProductAmount);

        });

        $('.take_or_pay').click(function () {
            var ProductId = this.id;
            var baseCharge = $('#base_charge_val_' + ProductId).text();
            var TakeorPay = $('#take_or_pay_val_' + ProductId).text();
            var TotalPrice = $('#product_price_' + ProductId).text();
            var ProductAmount = TotalPrice - TakeorPay;
            $('#input_product_id_' + ProductId).val(ProductId);
            $('#input_product_price_' + ProductId).val(ProductAmount);
            $('#input_product_price_type_' + ProductId).val('Base Charge');
            $('#take_or_pay_td_' + ProductId).text('');
            $('#base_charge_td_' + ProductId).find('a.base_charge').remove();
            ;
            $('#product_price_' + ProductId).text(ProductAmount);

        });

    });

</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.dataTables_filter input').attr('placeholder', 'Search...');
        var start = moment();
        var end = moment().add(1, 'days');
        $('input.single-daterange').daterangepicker({"startDate": start, "endDate": end});

        //DOM Manipulation to move datatable elements integrate to panel
        $('.panel-ctrls').append($('.dataTables_filter').addClass("pull-right")).find("label").addClass("panel-ctrls-center");
        $('.panel-ctrls').append("<i class='separator'></i>");
        $('.panel-ctrls').append($('.dataTables_length').addClass("pull-left")).find("label").addClass("panel-ctrls-center");

        $('.panel-footer').append($(".dataTable+.row"));
        $('.dataTables_paginate>ul.pagination').addClass("pull-right m-n");

        $('.date-calendar').datepicker({
            format: 'yyyy-mm-dd',

        });
        $('#date_from').datepicker({
            format: 'yyyy-mm-dd',

        });
        $('#date_to').datepicker({
            format: 'yyyy-mm-dd',

        });


        $('#example').DataTable({
            "ordering": false,
            "bDestroy": true,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            searching: false,
            dom: 'Bfrtip',
            buttons: [
                {
                    className: "btn btn-success btn-raised",
                    extend: 'csv',
                    text: 'Export',
                    exportOptions: {
                        modifier: {
                            search: 'none'
                        }
                    }
                },
            ]


        });

        $('#example_filter').hide();
        $('#defaultTable').DataTable({
            "ordering": false,
            dom: 'Bfrtip',
            buttons: [
                {
                    className: "btn btn-success btn-raised",
                    extend: 'csv',
                    text: 'Export',
                    exportOptions: {
                        modifier: {
                            search: 'none'
                        }
                    }
                }
            ],
        });
        $('#generate_coupon').click(function () {
            $('#coupon_code').val('CODE' + new Date().valueOf());
        });
        $("#select1,#select2,#select3").select2({width: '100%'});

    });

    function submitMe() {

        $('#submit2fidelity_form').submit();
        return true;
    }
</script>

<!--    Shopping Cart Js-->


</body>

</html>