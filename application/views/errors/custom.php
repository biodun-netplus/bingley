
        <div class="main">
				<div class="not-found-box">
					<div class="error-code">404</div>
					<div class="error-status">Page not Found</div>
					<div class="error-text">Oops, Something went wrong...</div>
					<div class="not-found-footer">
						<a class="btn btn-primary sm-max sm-mgtop-5" href="<?php echo site_url('dashboard') ?>"> Go back to homepage</a>
						
					</div>
				</div>
        </div>
