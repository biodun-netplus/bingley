<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>
    <title>FEDEATS | Error</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="template language" name="keywords">
    <meta content="Native Theme" name="author">
    <meta content="Admin Template" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="favicon.png" rel="shortcut icon">
    <link href="apple-touch-icon.png" rel="apple-touch-icon">

    <link rel="stylesheet" href="http://willcoonline.com.ng/bingley/assets/plugins/bootstrap/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="http://willcoonline.com.ng/bingley/assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://willcoonline.com.ng/bingley/assets/plugins/animate/animate.css">

    <link rel="stylesheet" href="http://willcoonline.com.ng/bingley/assets/plugins/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="http://willcoonline.com.ng/bingley/assets/plugins/bootstrap-daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="http://willcoonline.com.ng/bingley/assets/css/main.css" />

    <script src="http://willcoonline.com.ng/bingley/assets/plugins/jquery/jquery-2.1.1.min.js"></script>
    <script src="http://willcoonline.com.ng/bingley/assets/plugins/chart.js/dist/Chart.min.js"></script>
    <script src="http://willcoonline.com.ng/bingley/assets/plugins/jquery-count-to/jquery.countTo.js"></script>
    <script src="http://willcoonline.com.ng/bingley/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <script src="http://willcoonline.com.ng/bingley/assets/plugins/moment/min/moment.min.js"></script>
    <script src="http://willcoonline.com.ng/bingley/assets/plugins/select2/dist/js/select2.full.min.js"></script>
    <script src="http://willcoonline.com.ng/bingley/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="http://willcoonline.com.ng/bingley/assets/plugins/bootstrap-validator/dist/validator.min.js"></script>

    <script src="http://willcoonline.com.ng/bingley/assets/js/app.js"></script>
<style>
    textarea.form-control {
        height: auto;
    }
    </style>
</head>

<body>
    <div class="wrapper fixed-nav">
        <div class="main">
			<div class="not-found-box">
					<div class="error-code">404</div>
					<div class="error-status">Page not Found</div>
					<div class="error-text">Oops, Something went wrong...</div>
					<div class="not-found-footer">
						<a class="btn btn-primary sm-max sm-mgtop-5" href="<?php echo site_url('dashboard') ?>"> Go back to homepage</a>
						
					</div>
				</div>
        </div>
    </div>
</body>

</html>
