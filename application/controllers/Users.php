<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_model');
        $this->load->model('User_model');
        if (!$this->aauth->is_loggedin())
            redirect('login', 'refresh');
        if (!$this->aauth->is_member('Admin') && !$this->aauth->is_member('Report'))
            show_error('Access Denied');
    }

    public function index()
    {
        if (!$this->aauth->is_member('Admin'))
            show_error('Access Denied');

        $data = array(
            'title' => 'Product'
        );
        $TableNAme = 'product';
        $data['records'] = $this->Product_model->getRecord($TableNAme);
        $this->template->load('default', 'product/admin/index', $data);

    }

    public function admin($action = NULL, $id = NULL)
    {
        if (!$this->aauth->is_member('Admin'))
            show_error('Access Denied');

        $time = date('Y-m-d H:i:s');
        $data = array(
            'title' => 'Super Admin - Add'
        );
        if (isset($action) && $action == 'add') {
            $data['actionType'] = 'add';
            if ($this->input->post('save') == 'Save') {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
                $this->form_validation->set_rules('mobile_no', 'Mobile Number', 'trim|required');
                $this->form_validation->set_rules('email', 'Email', 'trim|required');
                $this->form_validation->set_rules('pass', 'Pass', 'trim|required');
                if ($this->form_validation->run() == FALSE) {
                    $this->session->set_flashdata('flashMsg', 'Fill All the field.');
                    redirect('users/admin/add/');
                } else {
                    $data = array(
                        'fullname' => $this->input->post('full_name', true),
                        'email' => $this->input->post('email', true),
                        'password' => $this->input->post('pass', true)
                    );
                    if ($userid = $this->aauth->create_user($data['email'], $data['password'])) {
                        if ($this->aauth->add_member($userid, $this->input->post('user_role', true))) {
                            $additional_data = array(
                                'full_name' => $this->input->post('full_name', true),
                                'mobile_no' => $this->input->post('mobile_no', true),
                            );
                            $this->aauth->aauth_db->where('id', $userid);
                            $this->aauth->aauth_db->update('aauth_users', $additional_data);
                            $this->session->set_flashdata('success', 'Account created successfully, please login.');
                            redirect('users/admin');
                        }
                    }

                }
            }
        } else if (isset($action) && $action == 'edit') {
            $data['actionType'] = 'edit';
            $conditation = array('id' => $id);
            $TableNAme = 'aauth_users';
            $data['records'] = $this->User_model->get_conditionData($conditation, $TableNAme);

            if ($this->input->post('save') == 'Save') {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
                $this->form_validation->set_rules('mobile_no', 'Mobile Number', 'trim|required');
                $this->form_validation->set_rules('email', 'Email', 'trim|required');

                if ($this->form_validation->run() == FALSE) {
                    $this->session->set_flashdata('flashMsg', 'Fill All the field.');
                    redirect('users/admin/add/');
                } else {
                    $new_user_group = $this->input->post('user_role', true);
                    $dataupdate = array(
                        'full_name' => $this->input->post('full_name', true),
                        'email' => $this->input->post('email', true),
                        'mobile_no' => $this->input->post('mobile_no', true)
                    );

                    $conditation = $id;
                    $TableNAme = 'aauth_users';
                    $data['records'] = $this->User_model->updateRecord($TableNAme, $dataupdate, $conditation);
                    //echo $this->bd>last_query();
                    $current_user_group = $this->aauth->get_user_groups($id)[0]->name;
                    if ($current_user_group !== $new_user_group) {
                        $this->aauth->add_member($id, $new_user_group);
                        $this->aauth->remove_member($id, $current_user_group);
                    }

                    $this->session->set_flashdata('success', 'Account created successfully, please login.');
                    redirect('users/admin');
                }
            }

        } else if (isset($action) && $action == 'delete') {

            $tableName = 'aauth_users';
            $conditation = array('id' => $id);
            if ($delete = $this->User_model->delete($tableName, $conditation)) {
                $tableName = 'aauth_user_to_group';
                $conditation1 = array('user_id' => $id);
                if ($delete = $this->User_model->delete($tableName, $conditation1)) {

                    $this->session->set_flashdata('success', 'Supder Admin Deleted Sucessfully');
                    redirect('users/admin');

                }

            }


        } else {
            $userType = ['Admin','Finance','Report'];
            if (!empty($this->input->post('date_search'))) {
                $date = $this->input->post('date_search');
                $data['records'] = $this->User_model->user_type_array_record($userType, $date);
            } else {
                $data['records'] = $this->User_model->user_type_array_record($userType);
            }


        }

        $this->template->load('default', 'users/admin/admin', $data);

    }


    public function regular_user($action = NULL, $id = NULL)
    {
        if (!$this->aauth->is_member('Admin'))
            show_error('Access Denied');

        $time = date('Y-m-d H:i:s');
        $data = array(
            'title' => 'Regular User - Add'
        );
        if (isset($action) && $action == 'add') {
            $data['actionType'] = 'add';
            if ($this->input->post('save') == 'Save') {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
                $this->form_validation->set_rules('mobile_no', 'Mobile Number', 'trim|required');
                $this->form_validation->set_rules('email', 'Email', 'trim|required');
                $this->form_validation->set_rules('pass', 'Pass', 'trim|required');
                if ($this->form_validation->run() == FALSE) {
                    $this->session->set_flashdata('flashMsg', 'Fill All the field.');
                    redirect('users/regular_user/add/');
                } else {
                    $data = array(
                        'fullname' => $this->input->post('full_name', true),
                        'email' => $this->input->post('email', true),
                        'password' => $this->input->post('pass', true)
                    );
                    $meter_no = $this->input->post('meter_no');
                    $check = $this->db->query("SELECT * FROM `aauth_users` WHERE `meter_no` = '$meter_no'");
                    if($check->num_rows() > 0 && $check->num_rows() < 2){
                        if($userid = $this->aauth->create_user($data['email'], $data['password'])){
                            if($this->aauth->add_member($userid,'Public')){
                                $additional_data =  array(
                                    'full_name' => $fullname,
                                    'meter_no' => $meter_no,
                                    'house_address' => $this->input->post('house_address',true),
                                    'type_of_ownership' => $this->input->post('type_of_ownership', true),
                                    'type_of_property' => $this->input->post('type_of_property',true),
                                    'mobile_no' => $this->input->post('mobile_no',true),
                                    'cug_no' => $this->input->post('cug_no',true),
                                    'partner_type' => 'Partner'
                                );
                                $to = $data['email'];
                                $subject = 'Registration Confirmation and Login Details';
                                
                                $config = array(
                                    'mailtype' => 'html',
                                    'protocol' => 'smtp',
                                    'smtp_host' => 'ssl://smtp.gmail.com',
                                    'smtp_user' => 'saddle@netplusadvisory.com',
                                    'smtp_pass' => 'Saddle7890',
                                    'smtp_port' => 465,
                                    'priority' => 1,
                                    'newline' => "\r\n"
                                );
        
        
                                $message = $this->load->view('email/registration_mail.php', $data, true);
        
                                $this->load->library('email', $config);
                                $this->email->initialize($config);
                                $this->email->set_newline("\r\n");
                                $this->email->from('saddle@netplusadvisory.com', 'Bingley Apartments');
                                $this->email->to($to);
        
                                $this->email->subject($subject);
                                $this->email->message($message);
        
                                $r = $this->email->send();
        
                                    
                                $this->aauth->aauth_db->where('id', $userid);
                                $this->aauth->aauth_db->update('aauth_users', $additional_data);
        
                            
                                $this->session->set_flashdata('success', 'Account created successfully, please login.');
                                redirect('users/regular_user');
                            }
                        }
                       
                    }elseif($check->num_rows() >= 2){
                        $this->session->set_flashdata('errors', "You can only register 2 users per room");
                        redirect('users/regular_user/add/', 'refresh');
                    }else{
                        if ($userid = $this->aauth->create_user($data['email'], $data['password'])) {
                            if ($this->aauth->add_member($userid, 'Public')) {
                                $additional_data = array(
                                    'full_name' => $this->input->post('full_name', true),
                                    'mobile_no' => $this->input->post('mobile_no', true),
                                    'meter_no' => $this->input->post('meter_no', true),
                                    'house_address' => $this->input->post('house_address', true),
                                    'cug_no' => $this->input->post('cug_no', true),
                                    'type_of_property' => $this->input->post('type_of_property', true),
                                    'type_of_ownership' => $this->input->post('type_of_ownership', true),
                                    'partner_type' => 'Partner'

                                );
                                $to = $data['email'];
                                $subject = 'Registration Confirmation and Login Details';


                                $config = array(
                                    'mailtype' => 'html',
                                    'protocol' => 'smtp',
                                    'smtp_host' => 'ssl://smtp.gmail.com',
                                    'smtp_user' => 'saddle@netplusadvisory.com',
                                    'smtp_pass' => 'Saddle7890',
                                    'smtp_port' => 465,
                                    'priority' => 1,
                                    'newline' => "\r\n"
                                );


                                $message = $this->load->view('email/registration_mail.php', $data, true);

                                $this->load->library('email', $config);
                                $this->email->initialize($config);
                                $this->email->set_newline("\r\n");
                                $this->email->from('saddle@netplusadvisory.com', 'Bingley Apartments');
                                $this->email->to($to);

                                $this->email->subject($subject);
                                $this->email->message($message);

                                $r = $this->email->send();
                                /*
                                $from = 'customer_service@lg3estate.com';

                                // To send HTML mail, the Content-type header must be set
                                $headers  = 'MIME-Version: 1.0' . "\r\n";
                                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                                // Create email headers
                                $headers .= 'From: LG3 Estate Association'."\r\n".
                                    'Reply-To: '.$from."\r\n" .
                                    'X-Mailer: PHP/' . phpversion();

                                // Compose a simple HTML email message

                                $message = $this->load->view('email/registration_mail.php',$data,TRUE);
                                mail($to, $subject, $message, $headers);
                                */

                                $this->aauth->aauth_db->where('id', $userid);
                                $this->aauth->aauth_db->update('aauth_users', $additional_data);

                                $type_of_property = $additional_data['type_of_property'];
                                $file = $this->db->query("SELECT * FROM `product` WHERE `property_type` = '$type_of_property' and `product_type` = 'Service'");
                                if($file->num_rows() > 0){
                                    $product = $file->result();
                                
                                    $meter_no = $additional_data['meter_no'];
                                    $product_id = $product[0]->id;
                                    $product_price = $product[0]->product_price;
                                    $this->db->query("INSERT INTO `service_charges` (`meter_no`, `user_id`, `product_id`, `amount_due`) VALUES ('$meter_no', '$userid', '$product_id', '$product_price') ");    
                                }
                                $this->session->set_flashdata('success', 'Account created successfully, please login.');
                                redirect('users/regular_user');
                            }
                        }
                    }

                }
            }
        } else if (isset($action) && $action == 'edit') {
            $data['actionType'] = 'edit';
            $conditation = array('id' => $id);
            $TableNAme = 'aauth_users';
            $data['records'] = $this->User_model->get_conditionData($conditation, $TableNAme);
            if ($this->input->post('save') == 'Save') {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
                $this->form_validation->set_rules('mobile_no', 'Mobile Number', 'trim|required');
                $this->form_validation->set_rules('email', 'Email', 'trim|required');

                if ($this->form_validation->run() == FALSE) {
                    $this->session->set_flashdata('flashMsg', 'Fill All the field.');
                    redirect('users/regular_user/add/');
                } else {

                    $conditation = $id;
                   
                    //CHECK IF THE USER METER IS FOR SOMEONE ELSE....update other data but not the meter number
                    $meter_no = $this->input->post('meter_no');
                    $check = $this->db->query("SELECT * FROM `aauth_users` WHERE `meter_no` = '$meter_no'");
                    if($check->num_rows() > 0){
                       
                        $uval = $check->result();
                        $u_id = $uval[0]->id;
       
                        $dataupdate = array(
                            'full_name' => $this->input->post('full_name', true),
                            'email' => $this->input->post('email', true),
                            'mobile_no' => $this->input->post('mobile_no', true),
                            'house_address' => $this->input->post('house_address', true),
                            'type_of_property' => $this->input->post('type_of_property', true),
                            'cug_no' => $this->input->post('cug_no', true),
                            'type_of_ownership' => $this->input->post('type_of_ownership', true)
                        );
                        $counter = count($uval);
                        for($i=0; $i < $counter; $i++){
                            
                            if($conditation == $uval[$i]->id){
                                $data['records'] = $this->User_model->updateRecord($TableNAme, $dataupdate, $conditation);
                            }else{

                                $dataupdate = array(
                                    'full_name' => $this->input->post('full_name', true),
                                    // 'email' => $this->input->post('email', true),
                                    // 'mobile_no' => $this->input->post('mobile_no', true),
                                    'house_address' => $this->input->post('house_address', true),
                                    'meter_no' =>  $this->input->post('meter_no', true),
                                    'type_of_property' => $this->input->post('type_of_property', true),
                                    // 'cug_no' => $this->input->post('cug_no', true),
                                    'type_of_ownership' => $this->input->post('type_of_ownership', true)
                                );
                                $data['records'] = $this->User_model->updateRecord($TableNAme, $dataupdate,  $conditation);
                            }
                           
                        }
                       
                        $property_type = $dataupdate['type_of_property'];
                        $pval = $this->db->query("SELECT * FROM `product` WHERE `property_type` = '$property_type' AND `product_type` = 'Service'")->result();
                        if(!empty($pval)){
                            $productId = $pval[0]->id;
                            $this->db->query("UPDATE `service_charges` SET `meter_no` = '$meter_no', `product_id` = '$productId' WHERE `user_id` = '$u_id'");

                            $this->session->set_flashdata('errors', 'Meter number already exist');
                            redirect('/users/regular_user', 'refresh');
                        }else{
                            redirect('/users/regular_user', 'refresh');
                        }
                    }else{
                        if (!empty($this->input->post('pass', true))) {
                            $this->aauth->update_user($id, false, $this->input->post('pass', true));
                        }
                        $dataupdate = array(
                            'full_name' => $this->input->post('full_name', true),
                            'email' => $this->input->post('email', true),
                            'mobile_no' => $this->input->post('mobile_no', true),
                            'house_address' => $this->input->post('house_address', true),
                            'type_of_property' => $this->input->post('type_of_property', true),
                            'cug_no' => $this->input->post('cug_no', true),
                            'meter_no' => $this->input->post('meter_no', true),
                            'type_of_ownership' => $this->input->post('type_of_ownership', true)
                        );
                       
                        $conditation = $id;
                        $TableNAme = 'aauth_users';
                        $data['records'] = $this->User_model->updateRecord($TableNAme, $dataupdate, $conditation);
                        $property_type = $dataupdate['type_of_property'];
                        $pval = $this->db->query("SELECT * FROM `product` WHERE `property_type` = '$property_type' AND `product_type` = 'Service'")->result();
                        $productId = $pval[0]->id;
                        $this->db->query("UPDATE `service_charges` SET `meter_no` = '$meter_no', `product_id` = '$productId' WHERE `user_id` = '$conditation'");

                        //$this->User_model->updateServiceCharge('service_charges', $meter_no, $conditation);
                        //echo $this->bd>last_query();
                        $this->session->set_flashdata('success', 'Account created successfully, please login.');
                        redirect('users/regular_user');
                   }


                   
                }
            }

        } else if (isset($action) && $action == 'delete') {

            $tableName = 'aauth_users';
            $conditation = array('id' => $id);
            if ($delete = $this->User_model->delete($tableName, $conditation)) {

                $tableName = 'aauth_user_to_group';
                $conditation1 = array('user_id' => $id);
                if ($delete = $this->User_model->delete($tableName, $conditation1)) {

                    $this->session->set_flashdata('success', 'Regular User Deleted Sucessfully');
                    redirect('users/regular_user');

                }

            }


        } else {
            $userType = 'Public';


            if (!empty($this->input->post('date_search'))) {
                $date = $this->input->post('date_search');
                $data['records'] = $this->User_model->user_type_record($userType, $date);
            } else {
                $data['records'] = $this->User_model->user_type_record($userType);

            }
        }

        $this->template->load('default', 'users/admin/regular_user', $data);

    }

    public function register_user($action = NULL, $id = NULL)
    {
        if (!$this->aauth->is_member('Admin'))
            show_error('Access Denied');

        $time = date('Y-m-d H:i:s');
        $data = array(
            'title' => 'Regular User - Add'
        );
        if (isset($action) && $action == 'edit') {
            $data['actionType'] = 'edit';
            $conditation = array('id' => $id);
            $TableNAme = 'aauth_users';
            $data['userInfo'] = $this->User_model->get_conditionData($conditation, $TableNAme);

            if ($this->input->post('save') == 'Save') {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
                $this->form_validation->set_rules('mobile_no', 'Mobile Number', 'trim|required');
                $this->form_validation->set_rules('email', 'Email', 'trim|required');

                if ($this->form_validation->run() == FALSE) {
                    $this->session->set_flashdata('flashMsg', 'Fill All the field.');
                    redirect('users/register_user/edit/' . $id);
                } else {

                    $dataupdate = array(
                        'full_name' => $this->input->post('full_name', true),
                        'email' => $this->input->post('email', true),
                        'mobile_no' => $this->input->post('mobile_no', true),
                        'house_address' => $this->input->post('house_address', true),
                        'type_of_property' => $this->input->post('type_of_property', true),
                        'type_of_ownership' => $this->input->post('type_of_ownership', true),
                        'meter_no' => $this->input->post('meter_no', true),
                        'partner_type' => $this->input->post('partner_type', true),
                        'pass' => $this->input->post('pass', true)
                    );

            

                    $conditation = $id;
                    $TableNAme = 'aauth_users';
                    $data['records'] = $this->User_model->updateRecord($TableNAme, $dataupdate, $conditation);
                    //echo $this->bd>last_query();
                    $this->session->set_flashdata('success', 'Account created successfully, please login.');
                    redirect('users/register_user');
                }
             
            }

        } else if (isset($action) && $action == 'delete') {

            $tableName = 'aauth_users';
            $conditation = array('id' => $id);
            if ($delete = $this->User_model->delete($tableName, $conditation)) {

                $tableName = 'aauth_user_to_group';
                $conditation1 = array('user_id' => $id);
                if ($delete = $this->User_model->delete($tableName, $conditation1)) {

                    $this->session->set_flashdata('success', 'Regular User Deleted Sucessfully');
                    redirect('users/register_user');

                }

            }


        }

        if (!empty($this->input->post('date_search'))) {
            $dateFrom = $this->input->post('date_from') . '00:00:00';
            $dateTo = $this->input->post('date_to') . '23:59:59';
            $data['records'] = $this->User_model->all_user($dateFrom, $dateTo);
        } else {

            $data['records'] = $this->User_model->all_user();
        }

        $this->template->load('default', 'users/admin/register_user', $data);

    }


    public function regulardirectory($action = NULL, $id = NULL)
    {
        $time = date('Y-m-d H:i:s');
        $data = array(
            'title' => 'Regular User - Add'
        );

        $userType = 'Public';


        if (!empty($this->input->post('date_search'))) {
            $date = $this->input->post('date_search');
            $data['records'] = $this->User_model->all_user();
        } else {
            $data['records'] = $this->User_model->all_user();

        }


        $this->template->load('default', 'users/admin/regulardirectory', $data);

    }

    public function userguide($action = NULL, $id = NULL){
        $this->template->load('default', 'users/admin/userguide');
    }

    public function partner_type()
    {
        if (!$this->aauth->is_member('Admin'))
            show_error('Access Denied');


        $partner_type = $this->input->post('partner_type');
        $User_id = $this->input->post('user_id');

        $product = $this->db->query("UPDATE aauth_users set partner_type = '" . $partner_type . "' WHERE id='" . $User_id . "'");
        redirect('users/register_user');

    }


    public function profile($UserId)
    {
        $data['title'] = 'User Profile';
        $conditation = array('id' => $UserId);
        $TableNAme = 'aauth_users';
        $data['records'] = $this->User_model->get_conditionData($conditation, $TableNAme);
        $this->template->load('default', 'users/admin/profile', $data);

    }

}
