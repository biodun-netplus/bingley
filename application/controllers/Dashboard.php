<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_model');
        $this->load->model('user_model');
		 $this->load->library('cart');
        if (!$this->aauth->is_loggedin())
            redirect('login', 'refresh');
    }

    public function index()
    {
        $payments = 0;
        $total_payments = 0;
        $last_payment = 0;
        $users = 0;
        $c_user = $this->aauth->get_user();
        $recent_payments = array();
        $outstanding_payments = array();
        $product = '';
		$dueAmount = '';
		$userList = '';
		
		 
		$type = '';
        if ($this->aauth->is_member('Merchant')) {
			

            $payments = $this->db->where('user_id', $c_user->id)->where('status')->count_all_results('payments');
            $query = $this->db->select_sum('amount_paid')->where('user_id', $c_user->id)->where('status','Paid')->get('payments');
            $row = $query->row();
            $total_payments = $row->amount_paid;

            $query = $this->db->where('user_id', $c_user->id)->where('amount_paid !=', '')->where('status','Paid')->limit(1)->order_by('date_created', 'desc')->get('payments');
            $row = $query->row();
            if($row){
                $last_payment = $row->amount_paid;
            }


            $query = $this->db->where('user_id', $c_user->id)->limit(3)->get('payments');
            $recent_payments = $query->result();
        }

        if ($this->aauth->is_member('Admin')) {
			
            $payments = $this->db->where('status', 'Paid')->count_all_results('payments');
            $query = $this->db->select_sum('amount_paid')->where('status','Paid')->where('payment_type','Card')->get('payments');
            $outstanding = $this->db->select_sum('amount')->where('status','1')->get('outstanding_bills');
            $bills = $outstanding->row();
            $total_bill_paid = 0;
            $total_other_payments = 0;
            $row = $query->row();
            $total_payments = $row->amount_paid;
            $users = count($this->user_model->regular_user());


            $prev_month_start = strtotime('first day of previous month', time());
            $prev_month_end = strtotime('last day of previous month', time());
            $prevStartDate = date('Y-m-d', $prev_month_start) . ' 00:00:00';
            $prevEndDate = date('Y-m-d', $prev_month_end) . ' 23:59:59';

            $date = date('Y-m-d h:i:s', time());
           
            // Update the system 
            $details = $this->user_model->regular_user();
            for($i=0; $i < $users; $i++){
                $property_type = $details[$i]->type_of_property;
                $user_id = $details[$i]->id;
                $query = $this->db->query("SELECT * FROM `service_charges` JOIN `product` ON `service_charges`.`product_id` = `product`.`id` WHERE `user_id` = '$user_id' AND `date_created` BETWEEN '$prevStartDate' and '$prevEndDate'")->result();
                if($query){
                    $serviceCharge = (int)$query[0]->product_price;
                    $advancePayment = (int)$query[0]->advance_payment; //Payed ahead 10000
                    $advanceMonth = (int)$query[0]->advance_month; // Paid for 2 months
                    $amountDue = (int)$query[0]->amount_due; // 5000
                    $productId = (int)$query[0]->product_id;
                    $totalPaid = (int)$query[0]->total_paid; // 15000
                    if($advancePayment > 0){
                        $amountPaid = $advancePayment / $advanceMonth;
                        $advancePayment = $advancePayment - $amountPaid;
                        $totalPaid = $amountPaid + $totalPaid;
                        $advanceMonth = $advanceMonth - 1;
                        $this->db->query("UPDATE `service_charges` SET `total_paid` = '$totalPaid', `advance_payment` = '$advancePayment', `advance_month` = '$advanceMonth', `date_created` = '$date' WHERE `user_id` = ' $user_id ' AND `product_id` = '$productId'");
                    }else{
                        if($amountDue > 0){
                            $amount = $serviceCharge + $amountDue;
                            $this->db->query("UPDATE `service_charges` SET `amount_due` = '$amount',  `date_created` = '$date' WHERE `user_id` = ' $user_id ' AND `product_id` = '$productId'");
                        }else{
                            $this->db->query("UPDATE `service_charges` SET `amount_due` = '$serviceCharge',  `date_created` = '$date' WHERE `user_id` = ' $user_id ' AND `product_id` = '$productId'");
                        }
                    }
                }
               

            }
            //$users = count($this->aauth->list_users());
            $outstanding_payments = $this->payment_model->outstanding(5);
            $recent_payments = $this->payment_model->all(5);
			$userList =  $ProPay = $this->db->query("SELECT * FROM  aauth_users ORDER BY  id DESC LIMIT 0 , 5")->result();
        }
		
		if ($this->aauth->is_member('Public')) {
		
             $user = $this->aauth->get_user();	
             
             $meter_no = $user->meter_no;

			$month_start = strtotime('first day of this month', time());
            $month_end = strtotime('last day of this month', time());
			$startDate =  date('Y-m-d', $month_start).' 00:00:00';
            $endDate = date('Y-m-d', $month_end).' 23:59:59';

             $product = $this->db->query("SELECT * FROM product where partner_type='".$user->partner_type."'")->result();
		     $this->db->where('meter_no',$meter_no);
             $payments = $this->db->count_all_results('payments');
             
             $where = "date_created BETWEEN '".$startDate."' and '". $endDate."'";
             //$query = $this->db->select_sum('amount')->where('status','Paid')->where('user_id',$user->id)->where('type','Security Charge')->where($where)->get('payments');
             $query = $this->db->select_sum('amount')->where('status','Paid')->where('meter_no',$meter_no)->where('type','Security Charge')->where($where)->get('payments');
             $row = $query->row();
            if($row->amount !== NULL){
                $total_security_payments = $row->amount;
            }else{
                $total_security_payments = 0;
            }

            //outstanding bills
            $query = $this->db->select_sum('amount')->where('status', '0')->where('user_id',$user->id)->get('outstanding_bills');
            $row = $query->row();
            if($row->amount !== NULL){
                $total_outstanding_bill = $row->amount;
            }else{
                $total_outstanding_bill = 0;
            }
         
            //outstanding bills payed
            $query = $this->db->select_sum('amount')->where('status', '1')->where('user_id',$user->id)->get('outstanding_bills');
            $row = $query->row();
            if($row->amount !== NULL){
                $total_bill_paid = $row->amount;
                if($total_outstanding_bill > 0){
                    $total_outstanding_bill = $total_outstanding_bill - $row->amount;
                }

            }else{
                $total_bill_paid = 0;
            }
    
             $query = $this->db->select_sum('amount')->where('status','Paid')->where('meter_no',$meter_no)->where('type','Service')->get('payments');
             $row = $query->row();
            if($row->amount !== NULL){
                $total_service_payments = $row->amount;
            }else{
                $total_service_payments = 0;
            }

             $query = $this->db->select_sum('amount')->where('status','Paid')->where('meter_no',$meter_no)->where('type','Water Pump')->get('payments');
             $row = $query->row();
            if($row->amount !== NULL){
                $total_water_payments = $row->amount;
            }else{
                $total_water_payments = 0;
            }
        

            $query = $this->db->select_sum('amount')->where('status','Paid')->where('meter_no',$meter_no)->where('type','Power')->get('payments');
            $row = $query->row();
            if($row->amount !== NULL){
                $total_power_payments = $row->amount;
            }else{
                $total_power_payments = 0;
            }
            
            $query = $this->db->select_sum('amount')->where('status','Paid')->where('meter_no',$meter_no)->where('type','Other')->get('payments');
            $row = $query->row();
            if($row->amount !== NULL){
                $total_other_payments = $row->amount;
            }else{
                $total_other_payments = 0;
            }
            
            
            $total_payments = $total_security_payments  + $total_water_payments;
            
			/*
			 $TotalAmountForPayment = $this->db->query("SELECT sum(product.product_price) as amountTotal
														FROM cart
														INNER JOIN product 
														ON cart.product_id = product.id 
														WHERE cart.order_id IS NOT NULL and cart.user_id='".$user->id."'")->row();
            */
            
            // $scp = $this->db->query("SELECT `amount_due` FROM `service_charges` WHERE `meter_no` = '$meter_no'")->row();
            // $serviceChargePayment = $scp->amount_due;

            $TotalAmountForServicePayment = $this->db->query("SELECT *
														FROM service_charges
                                                        WHERE meter_no = '".$meter_no."'")->row();

            if(isset($TotalAmountForServicePayment)){
                $dueServiceAmount = $TotalAmountForServicePayment->amount_due;
            }else{
                $dueServiceAmount = 0;
            }
            
            // if($serviceChargePayment >= $dueServiceAmount){
            //     $dueServiceAmount = $serviceChargePayment;
            // 

            $query = $this->db->query("SELECT `advance_payment` FROM `service_charges` WHERE `meter_no` = '$meter_no'");
            $row = $query->row();
            if($row != NULL){
                $advance_pay = (int)$row->advance_payment;
                if($advance_pay > 0){
                    $dueServiceAmount  = 0;
                }
            }  

        
            

            //$totalAmount = $this->db->query("SELECT * FROM `service_charges` WHERE `user_id` ='$user->id'")->row();
            // $total = $TotalAmountForServicePayment->amountTotal;
            // if($totalAmount === NULL){
            //     $totalAmount = 0;
            //     $TotalAmountForPayment = $totalAmount + $total;
            // }else{
            //     //$total = $TotalAmountForPayment->amountTotal;
            //     $TotalAmountForPayment = $totalAmount->amount_due + $total;
            // }
           
            // Total amount due power
            // $TotalAmountForPowerPayment = $this->db->query("SELECT sum(product_price) as amountTotal
			// 											FROM product
			// 											WHERE product_type='Power' AND partner_type = '" . $user->partner_type . "' AND property_type = '" . $user->type_of_property . "'")->row();
            // $duePowerAmount = $TotalAmountForPowerPayment->amountTotal;
           
             // Total amount due Water
             $TotalAmountForServicePayment = $this->db->query("SELECT sum(product_price) as amountTotal
             FROM product
             WHERE product_type='Water Pump' AND partner_type = '" . $user->partner_type . "' AND property_type = '" . $user->type_of_property . "'")->row();
            $dueWaterAmount = $TotalAmountForServicePayment->amountTotal;


            // Total amount security
            $TotalAmountForServicePayment = $this->db->query("SELECT sum(product_price) as amountTotal
            FROM product
            WHERE product_type='Security Charge' AND partner_type = '" . $user->partner_type . "' AND property_type = '" . $user->type_of_property . "'")->row();
            $dueSecurityAmount = $TotalAmountForServicePayment->amountTotal;
            
			//$dueAmount =  (($total_payments - $TotalAmountForPayment) > 0 ? 0 : $TotalAmountForPayment - $total_payments);
            $users = count($this->aauth->list_users('Merchant'));
            $recent_payments = $this->payment_model->all(3);
            $outstanding = $this->payment_model->outstanding(3);
           
            $dueAmount =  $dueServiceAmount + $dueWaterAmount + $dueSecurityAmount + $total_outstanding_bill;
     
       
            $dueAmount = $dueAmount - $total_payments;
           
            if($total_payments > $dueAmount){
                $dueAmount = $dueWaterAmount + $dueSecurityAmount;
            }

            if($total_water_payments > 0 ){
                $dueAmount = $dueAmount - $total_water_payments ;
            }

            if($total_security_payments > 0){
                $dueAmount = $dueAmount - $total_security_payments;
            }

            // if($total_power_payments > 0){
            //     $dueAmount = $dueAmount - $total_power_payments;
            // }
         
            // if($total_outstanding_bill > 0){
            //     $dueAmount = $dueAmount - $total_outstanding_bill;
            // }
            $total_payments = $total_payments + $total_power_payments  + $total_service_payments;
        }

        

        $data = array(
            'title' => 'Dashboard',
            'payments' => $payments,
            'total_payments' => $total_payments + $total_bill_paid +  $total_other_payments,
            'last_payment' => $last_payment,
            'users' => $users,
            'recent_payments' => $recent_payments,
            'outstanding_payments' => $outstanding_payments,
			'product'=>$product ,
			'dueAmount' =>$dueAmount,
			'userList' =>$userList
        );
		
        $this->template->load('default', 'account/dashboard', $data);

    }


    public function profile()
    {

        $data = array(
            'title' => 'Profile - Settings'
        );

        $this->template->load('default', 'account/settings', $data);

    }

    public function change_password()
    {

        $this->form_validation->set_rules('current_password', 'Current Password', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|matches[password]');

        $user = $this->aauth->get_user();

        if ($this->form_validation->run() == TRUE) {
            $current_pass = $this->input->post('current_password');
            $new_pass = $this->input->post('password');

            $password_hash = $this->aauth->hash_password($current_pass, $user->id);

            if ($this->aauth->verify_password($password_hash, $user->pass)) {

                if ($this->aauth->update_user($user->id, $email = FALSE, $new_pass, $username = FALSE)) {
                    $this->session->set_flashdata('success', 'Password updated successfully.');

                    redirect('profile/settings', 'refresh');
                }

            } else {
                $this->session->set_flashdata('errors', 'Current password entered is incorrect.');
            }


        }

        $errors = (validation_errors() ? validation_errors() : ($this->aauth->get_errors() ? $this->aauth->get_errors() : $this->session->flashdata('errors')));

        $this->session->set_flashdata('errors', $errors);

        redirect('profile/settings', 'refresh');

    }


    public function profile_update()
    {

        $c_user = $this->aauth->get_user();


        $this->form_validation->set_rules('fullname', 'Full Name', 'required');
        if ($this->aauth->is_member('Merchant')) {
           // $this->form_validation->set_rules('meter_no', 'Meter No', 'required');
			$this->form_validation->set_rules('mobile_no', 'Mobile No', 'required');
			$this->form_validation->set_rules('house_address', 'House Address', 'required');
			$this->form_validation->set_rules('type_of_property', 'Type of property', 'required');
        }
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

        $userid = $c_user->id;


        if ($this->form_validation->run() == TRUE) {


            if (!empty($this->input->post('password', true))) {
                $pass = $this->input->post('password', true);
            } else {
                $pass = FALSE;
            }

            if ($c_user->email == $this->input->post('email', true)) {
                $email = FALSE;
            } else {
                $email = $this->input->post('email', true);
            }


            if ($c_user->email != $this->input->post('email', true)) {
                if ($this->aauth->update_user($userid, $email, $pass) === FALSE) {

                    $this->session->set_flashdata('errors', $this->aauth->get_errors());
                    return redirect('profile/settings');

                }
            }

            $meter_no = $this->input->post('meter_no', true);

            $additional_data = array(
                'meter_no' => $meter_no,
                'full_name' => $this->input->post('fullname', true),
				'mobile_no' => $this->input->post('mobile_no', true),
				'house_address' => $this->input->post('house_address', true),
				'type_of_property' => $this->input->post('type_of_property', true),
				'cug_no' => $this->input->post('cug_no', true),
            ); 

            $this->aauth->aauth_db->where('id', $userid);

            if ($this->aauth->aauth_db->update('aauth_users', $additional_data)) {
                $this->session->set_flashdata('success', 'Your account has been updated successfully.');
            }

            return redirect('profile/settings', 'refresh');
        }

        $errors = (validation_errors() ? validation_errors() : ($this->aauth->get_errors() ? $this->aauth->get_errors() : $this->session->flashdata('errors')));

        $this->session->set_flashdata('errors', $errors);
        return redirect('profile/settings');
    }


    public function logout()
    {

        $this->aauth->logout();

        redirect('login', 'refresh');
    }

}