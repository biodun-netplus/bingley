<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
	}
    
    public function index(){
        $data = array(
            'title' => 'Error'
        );
        if ($this->aauth->is_loggedin())
            return $this->template->load('default','errors/custom',$data);
            
        return $this->load->view('errors/custom-not');
    }

}