<?php

class Payment_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

    }

    public function save($data)
    {
        $this->db->insert('payments', $data);
        $lastId = $this->db->insert_id();
        return $lastId;

    }

    public function get($where)
    {
        $this->db->select('*');
        $query = $this->db->get_where('payments', $where);
        return $query->row();
    }

    public function getAdminPayment()
    {
        $this->db->where('payments.admin_payment', 1);
        $this->db->order_by('payments.date_created','desc');
        $this->db->join('aauth_users', 'payments.user_id = aauth_users.id');
       $this->db->from('payments');
       $query = $this->db->get();  

       return $query->result();
    }

    public function getAdminPaymentByUserId($user_id)
    {
        $this->db->where('payments.admin_payment', 1);
        $this->db->where('payments.user_id', $user_id);
        $this->db->where('payments.status', 'Paid');
        $this->db->order_by('payments.date_created','desc');
        $this->db->join('aauth_users', 'payments.user_id = aauth_users.id');
       $this->db->from('payments');
       $query = $this->db->get();  

       return $query->result();
    }

    public function update($data,$id){
        $this->db->where('id', $id);
        if ($this->db->update('payments', $data)) {
            return true;
        }

        return false;
    }

    public function all($limit = FALSE,$offset = FALSE){
        if($limit){
            $this->db->limit($limit);
            if($offset)
                $this->db->limit($limit,$offset);
        }
        $this->db->where('payments.status', 'paid');
        $this->db->order_by('payments.date_created','desc');
		$this->db->join('cart', 'payments.payment_id = cart.order_id');
        $this->db->join('product', ' cart.product_id = product.id');
        $this->db->join('aauth_users', 'payments.user_id = aauth_users.id');
       $this->db->from('payments');
	   $query = $this->db->get();  

        return $query->result();
    }

    public function outstanding($limit = FALSE,$offset = FALSE){
        if($limit){
            $this->db->limit($limit);
            if($offset)
                $this->db->limit($limit,$offset);
        }
        $this->db->order_by('payments.date_created','desc');
        $this->db->join('cart', 'payments.payment_id = cart.order_id');
        $this->db->join('outstanding_bills', 'payments.payment_id = outstanding_bills.order_id');
        //$this->db->join('product', ' cart.product_id = product.id');
        $this->db->join('aauth_users', 'payments.user_id = aauth_users.id');
       $this->db->from('payments');
	   $query = $this->db->get();  
    
        return $query->result();
    }
}

?>