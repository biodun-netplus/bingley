<?php
class Coupon_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function save($data)
    {
       
        $this->db->insert('coupons', $data);
        $lastId = $this->db->insert_id();


        return $lastId;

    }

    public function check($data){
         $this->db->select('*')
            ->from('coupons')
            ->where('code', $data['code'])
            ->where('user_id', $data['user_id'])
            ->where('product_id', $data['product_id']);
        $query = $this->db->get();
        if($query->result())
            return false;
        return true;
    }

    public function verify($data){
         $this->db->select('*')
            ->from('coupons')
            ->where('code', $data['code'])
            ->where('user_id', $data['user_id'])
            ->where('product_id', $data['product_id'])
            ->where('status', 0);
        $query = $this->db->get();
        return $query->row();
    }

    public function all()
    {
        $this->db->select('coupons.*, product.product_name,aauth_users.full_name')
            ->from('coupons')
            ->join('product', 'coupons.product_id = product.id')
            ->join('aauth_users', 'coupons.user_id = aauth_users.id')
            ->order_by('coupons.created_at', 'desc');
        $query = $this->db->get();
        return $query->result();
    }

    public function get($id)
    {
        $this->db->select('*');
        $query = $this->db->get_where('coupons', array('id' => $id));
        return $query->row();
    }

    public function delete($id){
        $this->db->where('id',$id);
        $delete = $this->db->delete('coupons');
        return $delete;
    }

    public function update($data,$id){
        $this->db->where('id', $id);
        if ($this->db->update('coupons', $data)) {
            return true;
        }
        return false;
    }
}
